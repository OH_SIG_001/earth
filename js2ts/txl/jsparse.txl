% Utility transformation to test the JS/TS grammar
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

include "bom.grm"
include "typescript.grm"

define program
        [SourceFile] 
end define

function main
    % Simply parse and match the input as a JS/TS program
    match [program] P [program]
end function
