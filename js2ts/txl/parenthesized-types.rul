% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Parenthesized primary type inference
% Infer the types of parenthesized expressions from their contents

function parenthesizedExpressionTypes
    replace [program]
        P [program]
    by
        P [_parenthesizedExpressionType]
          [_parenthesizedCallExpressionType]
          [_parenthesizedSubscriptExpressionType]
          [_parenthesizedArrayExpressionType]
          [_parenthesizedSetExpressionType]
end function

% Private

rule _parenthesizedExpressionType
    % The type of a parenthesized expression (list) is the type of its (last) enclosed expression
    replace $ [PrimaryExpression]
        '( Expression [Expression] ') '(: OldType [Type] )
    % A parenthesized expression is a list of one or more assignment expressions
    % The type of the parenthesized expression is the type of the last (possibly only) one in the list
    skipping [AssignmentExpression]
    deconstruct * [AssignmentExpression,] Expression
        LastExpression [AssignmentExpression]
    % If it has a new type, update the parenthesized expression type
    deconstruct LastExpression
        _ [SubPrimaryExpression] '(: NewType [KnownType] )
    deconstruct not * [KnownType] OldType
        NewType
    by
        '( Expression ') '(: NewType )
end rule

rule _parenthesizedCallExpressionType
    % Similar to above, but if the last expression is a function call, the result type is the result type of the function
    replace $ [PrimaryExpression]
        '( Expression [Expression] ') '(: OldType [Type] )
    % Get the last expression
    skipping [AssignmentExpression]
    deconstruct * [AssignmentExpression,] Expression
        LastExpression [AssignmentExpression]
    % If it's a function call with a new result type, update the parenthesized expression type
    deconstruct LastExpression
        _ [SubPrimaryExpression] '(: '( _ [ParameterList] ') '=> ResultType [KnownType] ) ArgumentsOrTemplate [ArgumentsOrTemplate]
    deconstruct not * [KnownType] OldType
        ResultType
    by
        '( Expression ') '(: ResultType )
end rule

rule _parenthesizedSubscriptExpressionType
    % Similar to above, but if the last expression is a subscripted array, the result type is the element type of the array
    replace $ [PrimaryExpression]
        '( Expression [Expression] ') '(: OldType [Type] )
    % Get the last expression
    skipping [AssignmentExpression]
    deconstruct * [AssignmentExpression,] Expression
        LastExpression [AssignmentExpression]
    % If it's a subscripted array with a new element type, update the parenthesized expression type
    deconstruct LastExpression
        SubPrimaryExpression [SubPrimaryExpression] '(: UnionType [UnionType] ) '[ _ [Expression] ']
    deconstruct * [ArrayType] UnionType
        ElementType [PrimaryType] '[ ']
    deconstruct not OldType
        ElementType
    by
        '( Expression ') '(: ElementType )
end rule

rule _parenthesizedArrayExpressionType
    % If we have inferred the element type of a subscripted array and don't know the array type, update it
    replace $ [PrimaryExpression]
        '( SubPrimaryExpression [SubPrimaryExpression] '(: 'any ) '[ SubscriptExpression [Expression] ']  ')
            '(: ElementType [PrimaryType] ')
    % Iterators are not arrays
    deconstruct not SubscriptExpression
        'Symbol.iterator _ [ExpressionType?]
    % Boolean is a weak inference, so we don't want to propagate it
    deconstruct not ElementType
        'boolean
    % If the subscripted item is a string, it's not an array
    deconstruct not ElementType
        'string
    % Otherwise, we can infer the subscripted array type or string from the known element type
    construct InferredArrayType [KnownType]
        ElementType '[ ']
    by
        '( SubPrimaryExpression '(: InferredArrayType [_unlessString ElementType] ) '[ SubscriptExpression '] ') '(: ElementType ')
end rule

function _unlessString ElementType [PrimaryType]
    % If the element type is string, the subscript is probably a substring selection, not an array subscript
    replace [KnownType]
        'string '[ ']
    by
        'string
end function

rule _parenthesizedSetExpressionType
    % If we know the type of the array elements in a Set constructor, we can infer the element type of the Set
    replace $ [PrimaryExpression]
        '( 'new 'Set '(: 'Set) '( ArrayLiteral [ArrayLiteral] '(: ElementType [PrimaryType] '[ '] ') ') ') '(: 'Set '< 'any '> ')
    by
        '( 'new 'Set '(: 'Set) '( ArrayLiteral '(: ElementType '[ '] ') ') ') '(: 'Set '< ElementType '> ')
end rule
