% JS2TS - convert JavaScript to TypeScript using type inference
% James Cordy, Huawei Technologies
% May 2022 (Rev. Aug 2023)

% Literal expression type inference rules

% Infer literal expression types from the kind of literal

function literalTypes
    replace [program]
        Program [program]
    by
        Program [_nullLiteralTypes]
                [_booleanLiteralTypes]
                [_numericLiteralTypes]
                [_bigintLiteralTypes]
                [_stringLiteralTypes]
                [_templateLiteralTypes]
                [_arrayLiteralTypes]
                [_undefinedLiteralTypes]
                [_regexpLiteralTypes]
end function

function computedArrayLiteralTypes
    replace [program]
        Program [program]
    by
        Program [_arrayLiteralTypes]
end function

% Private

rule _arrayLiteralTypes
    % Annotate an array literal, e.g., [1,2,3] with its array type, e.g., number[]
    % Note that we cannot do this only once - it may change as we infer types of elements, e.g., [a,b,c]
    replace $ [PrimaryExpression]
        ArrayLiteral [ArrayLiteral] '(: OldType [Type] ')
    % Recursively aannotate sub-array literals
    construct DeepArrayLiteral [ArrayLiteral]
        ArrayLiteral [_arrayLiteralTypes]
    % The default element type is "any"
    construct DefaultArrayElementType [PrimaryType]
        'any
    % If it has consistent element types, then we know the element type of the array
    construct ArrayElementType [PrimaryType]
        DefaultArrayElementType [_getArrayElementType DeepArrayLiteral]
    construct ArrayType [Type]
        ArrayElementType '[ ']
    % Is it new, or did we already know it?
    deconstruct not OldType
        ArrayType
    by
        DeepArrayLiteral '(: ArrayType ')
end rule

function _getArrayElementType ArrayLiteral [ArrayLiteral]
    % Replace the default element type "any" by the type of the elements,
    % if they are all consistent
    deconstruct ArrayLiteral
        '[ Elements [list Element+] Elision [Elision?] ']
    % Begin with the type of the first element
    deconstruct * [Element] ArrayLiteral
         _ [SubPrimaryExpression] '(: PrimaryType [PrimaryType] )
    % Are the others all that same type?
    where all
        PrimaryType [_matchArrayElementType each Elements]
    % If so, we know the element type of the array
    replace [PrimaryType]
        'any
    by
        PrimaryType
end function

function _matchArrayElementType Element [Element]
    % Check that each element has the same type as the first element
    match [PrimaryType]
        PrimaryType [PrimaryType]
    % Is it the same?
    deconstruct Element
        _ [SubPrimaryExpression] ( : PrimaryType )
end function

rule _nullLiteralTypes
    % "null" has its own type
    replace $ [PrimaryExpression]
        NullLiteral [NullLiteral] '(: 'any )
    by
        NullLiteral '(: 'null )
end rule

rule _booleanLiteralTypes
    % "true" and "false" are both of type boolean
    construct Booleans [id*]
        'true 'false
    % Give every "true" or "false" type boolean
    replace $ [PrimaryExpression]
        TrueFalse [id] '(: 'any )
    deconstruct * [id] Booleans
        TrueFalse
    by
        TrueFalse '(: 'boolean )
end rule

rule _numericLiteralTypes
    % Numeric literals are of type number
    replace $ [PrimaryExpression]
        NumericLiteral [NumericLiteral] '(: 'any )
    by
        NumericLiteral '(: 'number )
end rule

rule _bigintLiteralTypes
    % Bigint literals are of type bigint
    % Note: normalize-expressions.rul has normalized "42n" to "bigint(42)",
    % and parenthesize-expressions.rul has parenthesized it so that it has an expression type
    replace $ [PrimaryExpression]
        ( 'bigint ( :  'any ) Arguments [Arguments] ) ( : 'any )
    % Avoid type argument ambiguity
    construct PrimaryBigInt [PrimaryExpression]
        'bigint ( : 'bigint )
    by
        ( PrimaryBigInt Arguments ) ( : 'bigint )
end rule

rule _stringLiteralTypes
    % String literals, e.g., "abcd", 'abcd', are of type string
    replace $ [PrimaryExpression]
        StringLiteral [StringLiteral] '(: 'any )
    by
        StringLiteral '(: 'string )
end rule

rule _templateLiteralTypes
    % Template literals, e.g., `ab${c}`, are also of type string
    replace $ [PrimaryExpression]
        TemplateLiteral [TemplateLiteral] '(: 'any )
    by
        TemplateLiteral '(: 'string )
end rule

rule _undefinedLiteralTypes
    % "undefined" has its own type
    replace $ [PrimaryExpression]
        'undefined '(: 'any )
    by
        'undefined '(: 'undefined )
end rule

rule _regexpLiteralTypes
    % Regular expression literals, e.g., /abc/, are objects of the built-in class RegExp
    replace $ [PrimaryExpression]
        RegularExpressionLiteral [RegularExpressionLiteral] '(: 'any )
    by
        RegularExpressionLiteral '(: 'RegExp )
end rule
