% JS2TS - convert JavaScript to TypeScript using type inference
% Jim Cordy, Huawei Technologies
% May 2022 (Rev. June 2022)

% Keyword renaming rules

% Some identifiers in JS become keywords in TS, so we need to rename them

function renameKeywords
    % TS keywords that are not JS keywords, and their renamed forms
    export KeywordIds [id*]
        'number
    export RenamedIds [id*]
        'number_
    % Rename all instances of them in both code and templates
    replace [program]
        P [program]
    by
        P [_renameKeywords]
          [_renameEmbeddedKeywords]
end function

% Private

function _renameKeywords
    % Rename each TS keyword that appears as a JS identifier
    import KeywordIds [id*]
    import RenamedIds [id*]
    replace [program]
        P [program]
    by
        P [$ each KeywordIds RenamedIds]
end function

rule _renameEmbeddedKeywords
    % Do the same for those embedded inside templates
    import KeywordIds [id*]
    import RenamedIds [id*]

    replace $ [SubstitutionTemplate]
        Template [SubstitutionTemplate]                 % `${number} ${mutation(format[key], number)}`
    % Get the text of the template as a stringlit
    construct TemplateString [stringlit]
        _ [+ Template]                                  % "${number} ${mutation(format[key], number)}"
    % Get a list of the embedded code expressions
    construct EmbeddedCodes [stringlit*]
        _ [_getEmbeddedCodes TemplateString]            % "${number}" "${mutation(format[key], number)"
    % Make a renamed copy of the list
    construct RenamedEmbeddedCodes [stringlit*]
        EmbeddedCodes [_renameEmbeddedKeyword each KeywordIds RenamedIds]    % "${number_}" "${mutation(format[key], number_)"
    % Did we rename any of them?
    deconstruct not RenamedEmbeddedCodes
        EmbeddedCodes
    % Yes, so substitute them in the template string
    construct RenamedTemplateString [stringlit]
        TemplateString [substglobal each EmbeddedCodes RenamedEmbeddedCodes] % "${number_} ${mutation(format[key], number_)}"
    by
        _ [+ RenamedTemplateString]                     % `${number_} ${mutation(format[key], number_)}`
end rule

rule _renameEmbeddedKeyword KeywordId [id] RenamedId [id]
    % Use the TXL stringutils library to use string substitution
    replace $ [stringlit]
        EmbeddedCode [stringlit]
    construct KeywordIdString [stringlit]
        _ [+ KeywordId]
    construct RenamedIdString [stringlit]
        _ [+ RenamedId]
    by
        % vim: s/number/number_/g
        EmbeddedCode [substglobal KeywordIdString RenamedIdString]
end rule

function _getEmbeddedCodes TemplateString [stringlit]
    % Find the first embedded code expression in the template string
    construct ECindex [number]              % " ... ${number} ... ${mutation(format[key], number)} ..."
        _ [index TemplateString "${"]
    deconstruct not ECindex
        0
    % Get the embedded code expression text
    construct EChead [stringlit]
        TemplateString [: ECindex 9999]     % "${number} ... ${mutation(format[key], number)} ..."
    construct EndECindex [number]
        _ [index EChead "}"]
    deconstruct not EndECindex
        0
    construct EC [stringlit]
        EChead [: 1 EndECindex]             % "${number}"
    % Get the rest of the template string, in case there are more embedded code expressions in it
    construct EndECindexP1 [number]
        EndECindex [+ 1]
    construct PostEC [stringlit]
        EChead [: EndECindexP1 9999]        % " ... ${mutation(format[key], number)} ..."
    % Add this one to the list
    replace * [stringlit*]
        ECs [stringlit*]
    by
        ECs [. EC]                          % Add this one to the list
            [_getEmbeddedCodes PostEC]      % And look for more in the rest of the template string
end function

% We use the TXL string utility library
include "stringutils.rul"

