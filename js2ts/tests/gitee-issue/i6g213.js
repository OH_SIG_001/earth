function* g() {
    yield 123;
    yield "abc";
}
var iter = g();
var result1;
var result2;
result1 = iter.next();
console.log(result1.value);
result2 = iter.next();
console.log(result2.value);
