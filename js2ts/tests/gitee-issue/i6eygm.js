var fooCalled = false;
function foo1() { 
    fooCalled = true; 
} 
var o1 = {};
o1.bar.gar( foo1() );

var o2 = { }; 
Object.defineProperty(o2, "bar", {get: function()  {this.barGetter = true; return 42;}, 
                                  set: function(x) {this.barSetter = true; }});
o2.foo1(o2.bar);


var foo2;
(function() {
  let x = "inside";
  foo2 = function() { return x; };
}());
var x = 123;
console.log(foo2());
console.log(x);

var callCount = 0;
(function() {
  callCount += 1;
}(5, ...[6, 7, 8]));
console.log(callCount);

