const littleEndian = (() => {
  const buffer = new ArrayBuffer(2);
  var dataview =  new DataView(buffer);
  dataview.setInt16(0, 256, true);
  var ret = new Int16Array(buffer)[0] === 256;
  return ret;
})();
console.log(littleEndian);

var buf = new ArrayBuffer(8);
console.log(buf);
var dv = new DataView(buf);
console.log(dv);

function foo(){
  let b = new ArrayBuffer(4);
  let d = new DataView(b);
  console.log(b);
  console.log(d);
}

foo();
