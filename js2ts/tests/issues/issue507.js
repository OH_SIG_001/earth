// import {abs, acos, asin, atan2, cos, epsilon, halfPi, max, min, pi, sin, sqrt, tau} from "./math.js";
epsilon = 0.0000001
function intersect(x0, y0, x1, y1, x2, y2, x3, y3) {
    var x10 = x1 - x0, y10 = y1 - y0,
    x32 = x3 - x2, y32 = y3 - y2,
    t = y32 * x10 - x32 * y10;
    if (t * t < epsilon) return;
    t = (x32 * (y0 - y2) - y32 * (x0 - x2)) / t;
    return [x0 + t * x10, y0 + t * y10];
}

var x01 = 0.009,
y01 = 0.008,
x10 = 0.09,
y10 = 0.8;

var x11 = 0.9,
y11 = 0.008,
x00 = 0.07,
y00 = 0.2,
oc;

if (oc = intersect(x01, y01, x00, y00, x11, y11, x10, y10)) {
    console.log("Hello");
}
