let text = '{ "employees" : [' +
'{ "firstName":"John" , "lastName":"Doe" },' +
'{ "firstName":"Anna" , "lastName":"Smith" },' +
'{ "firstName":"Peter" , "lastName":"Jones" } ]}';
const obj = JSON.parse(text);
console.log(typeof(obj));

var v1 = JSON.stringify({ x: 5, y: 6 });
console.log(v1);
console.log(typeof(v1));

var v2 = JSON.stringify([new Number(3), new String('false'), new Boolean(false)]);
console.log(v2);

var v3 = JSON.stringify({ x: [10, undefined, function(){}, Symbol('')] });
console.log(v3);

var v4 = JSON.stringify(new Date(2006, 0, 2, 15, 4, 5));
console.log(v4);
