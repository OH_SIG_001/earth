'use strict';

var doc = { cookie : "x=4" };

function read_cookie(name) {
    var result = doc.cookie.match(new RegExp(name + '=([^;]+)'));
    result = result != null ? JSON.parse(result[1]) : [];
    return result;
}

console.log (read_cookie ("x"));
