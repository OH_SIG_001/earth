function mapping(t) {
    var psi = t * psiMax;
    return (
        (2 * psi +
        (1 + a - b / 2) * sin(2 * psi) +
        ((a + b) / 2) * sin(4 * psi) +
        (b / 2) * sin(6 * psi)) /
        psiMax
    );
}
