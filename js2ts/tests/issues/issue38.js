//this is a single-line comment
/* This
    is
    a
    multi-line
    comment */

var num = 0; // num is the number of total students

function Circle(radius /* any */)  { //the embedded comment is difficult to kept

}
var obj = {name:"Alice",
        //another embedded comment, parsing error
        age:13,
        location:"Toronto"};

console.log(obj);
