const buffer = new ArrayBuffer(8, { maxByteLength: 16 });
const float32 = new Float32Array(buffer);

const i=float32.byteLength;
console.log(i); // 8
const j=float32.length;
console.log(j); // 2

const uint8array1 = new Uint8Array(buffer);
const k=uint8array1.byteOffset; // 0 (no offset specified)

const uint8array2 = new Uint8Array(buffer, 3);
const l=uint8array2.byteOffset;

const m =Float64Array.BYTES_PER_ELEMENT;
const buffer2 = new ArrayBuffer(8);
const uint16 = new Uint16Array(buffer2);
const u= uint16.buffer;

console.log(uint16.buffer.byteLength);
