// Create a new object to be referenced
var obj = { name: 'John' };

// Create a new WeakRef instance that references the object
const ref = new WeakRef(obj);

// Check if the WeakRef instance still has a reference to the object
var dref = ref.deref();
console.log(dref === obj); // true

// Remove the reference to the object
obj = null;

// Check if the WeakRef instance still has a reference to the object
dref = ref.deref();
console.log(dref === obj); // false

// Call the cleanupSome method to force garbage collection
globalThis.gc?.();

// Check if the WeakRef instance still has a reference to the object after garbage collection
dref = ref.deref();
console.log(dref === obj); // false, since the object has been garbage collected
