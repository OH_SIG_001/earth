function accurateInterval(func, interval, opts) {

    if (!opts) opts = {};

    var clear, nextAt, timeout, wrapper, now;

    now = new Date().getTime();

    nextAt = now;

    if (opts.aligned) {
        nextAt += interval - (now % interval);
    }
    if (!opts.immediate) {
        nextAt += interval;
    }

    timeout = null;

    wrapper = function wrapper() {
        var scheduledTime = nextAt;
        nextAt += interval;
        timeout = setTimeout(wrapper, nextAt - new Date().getTime());
        func(scheduledTime);
    };

    clear = function clear() {
        return clearTimeout(timeout);
    };

    timeout = setTimeout(wrapper, nextAt - new Date().getTime());

    return {
        clear: clear
    };

  };
  console.log('Start time:  '  +Date.now());
  var foo = accurateInterval(function(scheduledTime) {
      console.log('Actual time: ' + Date.now() + ' -- Scheduled time: ' + scheduledTime);
  }, 2000, {aligned: true, immediate: true});
  
  
  setTimeout(function() {
      foo.clear();
  }, 10000);
