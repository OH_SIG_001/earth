function cornerTangents(x0, y0, x1, y1, r1, rc, cw) {
  return {
    cx: 1,
    cy: 1,
    x01: -1,
    y01: -1,
    x11: 0.9,
    y11: 0.8
  };
}

var r1=9,
    rc1=2,
    cw=9,
    x01 =0.9,
    y01 =0.8,
    x10=4,
    y10=9;

if (9<x01) {
    console.log ("case 1");
    var x00 = 7,
        y00 =9,
        x11 =8,
        y11 =6;
} else {
    console.log ("case 2");
    console.log ("x00 is " + x00);
    console.log ("x11 is " + x11);
    console.log ("y00 is " + y00);
    console.log ("y11 is " + y11);
    t0 = cornerTangents(x00, y00, x01, y01, r1, rc1, cw);
    t1 = cornerTangents(x11, y11, x10, y10, r1, rc1, cw);
}

console.log (t0)
console.log (t1)
