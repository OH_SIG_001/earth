// Test Function constructor
const func = new Function("a", "b", "return a + b;");
console.log(func(3, 4)); // 7

// Test Function properties
const exampleFunction = function example(a, b) {
  return a * b;
};

let _len = exampleFunction.length;
console.log(_len); // 2 (number of formal parameters)
let _name = exampleFunction.name;
console.log(_name); // "example" (function name)

// Test Function.prototype
const proto = Object.getPrototypeOf(exampleFunction);
console.log(proto === Function.prototype); // true

// Test Function.apply()
var _apply = exampleFunction.apply(null, [3, 4]);
console.log(_apply); // 12

// Test Function.bind()
const boundFunction = exampleFunction.bind(null, 5);
console.log(boundFunction(6)); // 30

// Test Function.call()
let _call = exampleFunction.call(null, 3, 4);
console.log(_call); // 12

// Test Function.toString()
var _tostring = exampleFunction.toString();
console.log(_tostring);
/*
  "function example(a, b) {
    return a * b;
  }"
*/
