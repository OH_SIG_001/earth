// #76
var f = async () => {
    return 1;
};

var g = async () => {
    return "abc";
};

var h = async (x, y, z) => {
    return x + y + z;
};

h(1, 2, 3);

// Secondary case - what if the arrow function can't be normalized?
var j = (async (x, y) => { return x + y; })(40, 2);
console.log (j);
