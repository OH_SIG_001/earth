// multi-way non-boolean logical operators
let s = "abc";
let n = 42;
let b = 42n;
let t = true;

// two-way 
let ss = s || s;
let sn = s || n;
let sb = s || b;
let st = s || t;
let ns = n || s;
let bs = b || s;
let ts = t || s;

// three-way direct
let sss = s || s || s;
let sns = s || n || s;
let sbs = s || b || s;
let sts = s || t || s;

let ssn = s || s || n;
let snn = s || n || n;
let sbn = s || b || n;
let stn = s || t || n;

let ssb = s || s || b;
let snb = s || n || b;
let sbb = s || b || b;
let stb = s || t || b;

let sst = s || s || t;
let snt = s || n || t;
let sbt = s || b || t;
let stt = s || t || t;

// three-way indirect
let siss = ss || s;
let sins = sn || s;
let sibs = sb || s;
let sits = st || s;

let sisn = ss || n;
let sinn = sn || n;
let sibn = sb || n;
let sitn = st || n;

let sisb = ss || b;
let sinb = sn || b;
let sibb = sb || b;
let sitb = st || b;

let sist = ss || t;
let sint = sn || t;
let sibt = sb || t;
let sitt = st || t;
