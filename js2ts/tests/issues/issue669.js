"use strict";
var NamespaceExample;
(function (NamespaceExample) {
    // class Hewan {}
    // const kucing = new Hewan();
    class Hewan {
        constructor() {
            this.name = "Kucing Putih";
        }
    }
    NamespaceExample.Hewan = Hewan;
    NamespaceExample.kucing = new Hewan();
    class Animal {
        constructor() {
            this.name = "Burung Kenari";
        }
    }
    NamespaceExample.Animal = Animal;
})(NamespaceExample || (NamespaceExample = {}));
const kenariBurung = new NamespaceExample.Animal();
console.log(kenariBurung);
const kucingPutih = NamespaceExample.kucing;
console.log(kucingPutih);
