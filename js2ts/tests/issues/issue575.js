MAX_DP = 1E6,
    NAME = '[big.js] ',
    INVALID = NAME + 'Invalid ',
    INVALID_DP = INVALID + 'decimal places',
    INVALID_RM = INVALID + 'rounding mode',
    DIV_BY_ZERO = NAME + 'Division by zero',
    P = {},
    UNDEFINED = void 0,
    NUMERIC = /^-?(\d+(\.\d*)?|\.\d+)(e[+-]?\d+)?$/i;
function round(x, sd, rm, more) {
    var xc = x.c;
    if (rm === UNDEFINED)
        rm = x.constructor.RM;
    if (sd < 1) {
        more =
            rm === 3 && (more || !!xc[0]) || sd === 0 && (rm === 1 && xc[0] >= 5 ||
                rm === 2 && (xc[0] > 5 || xc[0] === 5 && (more || xc[1] !== UNDEFINED)));
        xc.length = 1;
        if (more) {
            x.e = x.e - sd + 1;
            xc[0] = 1;
        }
        else {
            xc[0] = x.e = 0;
        }
    }
    return x;
}
function Big() { }
P = Big.prototype;
P.prec = function (sd, rm) {
    if (sd !== ~~sd || sd < 1 || sd > MAX_DP) {
        throw Error(INVALID + 'precision');
    }
    return round(new this.constructor(this), sd, rm);
};
P.toExponential = function (dp, rm) {
    var x = this;
    if (dp !== UNDEFINED) {
        if (dp !== ~~dp || dp < 0 || dp > MAX_DP) {
            throw Error(INVALID_DP);
        }
        x = round(new x.constructor(x), ++dp, rm);
        return x;
    }
};
P.toFixed = function (dp, rm) {
    var x = this;
    if (dp !== UNDEFINED) {
        if (dp !== ~~dp || dp < 0 || dp > MAX_DP) {
            throw Error(INVALID_DP);
        }
        x = round(new x.constructor(x), dp + x.e + 1, rm);
        return x;
    }
};
var x = new Big();
console.log(x.prec(1, 2));
console.log(x.toFixed(1, 2));
console.log(x.toExponential(1, 2));
