for (i=1; i<10; i++)    {       // global i
}

console.log (i);

function f () {
    for (i=1;i<20;i++) {        // global i
    }
    console.log (i);
    {
        for (i=1;i<30;i++) {    // global i
        }
    }
    console.log (i);
}

f();
console.log (i);

function g () {
    for (var i=1;i<40;i++) {    // function i
    }
    console.log (i);
    {
        for (i=1;i<50;i++) {    // function i
        }
    }
    console.log (i);
}

g();
console.log (i);

function h () {
    {
        for (let i=1;i<60;i++) {    // local i
        }
        console.log (i);
        {
            for (i=1;i<70;i++) {    // local i
            }
        }
        console.log (i);
    }
    console.log (i)
}

h();
console.log (i);
