var x = 42;
var y = "abc";

var f1 = function (x,y) { 
    if (x == 1) {
        let y = 42;
        x = 42;
        console.log (y);
    }
    console.log (x);
    console.log (y);
}

function f2 (x,y) { 
    x = "abc"; 
    y = y + 1;
    if (y == 42) {
        let x = false;
        x = !x;
        console.log (x);
    }
    console.log (x);
    console.log (y);
}

f1(1,"abc");
f2("abc",41);

class c1 {
    f1 = function (x,y) { 
        x = !x; 
        y = y + "c";
        console.log(y);
    }

    f2 (x,y) { 
        x = 42; 
        y = y || false;
        console.log(y);
    }
}

var o1 = new c1();
o1.f1(true,"ab");
console.log(x);
o1.f2(1,true);
console.log(y);
