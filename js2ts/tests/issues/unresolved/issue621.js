function cb(a, b) {
    return function (c) {
            return a + b + c
    }

}

function foo (a, b, c) {
    a = b
    b = c
    c = false
    if (c == Boolean(null)) {
        b = cb(a, b)
    }
    var d = b ? b(1) : 4
    return d
}
console.log(foo(4, 5, 6))
