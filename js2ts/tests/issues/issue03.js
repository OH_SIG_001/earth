// Issue with retaining redundant type annotations in output 
// ***OBSOLETE** 
class Point2d {
    constructor (x,y) {
	this.x = x;
	this.y = y;
    }

    distance () {
 	if (this.x != 0)
	    if (this.x > this.y)
		    return this.x
	    else
		    return this.y;
    else
        return this.y;
    }
}

// Output for next line should be:  const x = 1;  not  const x : number = 1;
const x = 1;

// Output for next line should be:  let y = 1;  not  let y : number = 1;
let y = 2.0;

// Output for next line should be:  let p = new Point2d (x, y);  not let p : Point2d = new Point2d (x, y);
let p = new Point2d (x, y);

function foo (v) {
    return v.distance ();
}

// Output for next line should be :  var d = foo (p);  not  var d : number = foo (p)
let d = foo (p);

console.log (d);
