type JSDynamicObject = any;
var obj : JSDynamicObject = {
    name : 'John'
};
const ref : WeakRef <any> = new WeakRef <any> (obj);
var dref : JSDynamicObject = ref.deref ();
console.log (dref === obj);
obj = null;
dref = ref.deref ();
console.log (dref === obj);
globalThis.gc ?.();
dref = ref.deref ();
console.log (dref === obj);
