type JSNoValue = any;
type JSDynamicObject = any;
"use strict";
exports.__esModule = true;
exports.Test262Error = exports.assert = void 0;
function assert (mustBeTrue : boolean, message : string) : JSDynamicObject | JSNoValue {
    if (mustBeTrue === true) {
        return;
    }
    if (message === undefined) {
        message = 'Expected true but got ' + String (mustBeTrue);
    }
    $ERROR (message);
}
exports.assert = assert;
assert._isSameValue = function (a : number, b : number) : boolean {
    if (a === b) {
        return a !== 0 || 1 / a === 1 / b;
    }
    return a !== a && b !== b;
}
;
assert.sameValue = function (actual : any, expected : any, message : string) : void {
    if (assert._isSameValue (actual, expected)) {
        return;
    }
    if (message === undefined) {
        message = '';
    } else {
        message += ' ';
    }
    message += 'Expected SameValue(«' + String (actual) + '», «' + String (expected) + '») to be true';
    $ERROR (message);
}
;
assert.notSameValue = function (actual : any, unexpected : any, message : string) : void {
    if (! assert._isSameValue (actual, unexpected)) {
        return;
    }
    if (message === undefined) {
        message = '';
    } else {
        message += ' ';
    }
    message += 'Expected SameValue(«' + String (actual) + '», «' + String (unexpected) + '») to be false';
    $ERROR (message);
}
;
assert.throws = function (expectedErrorConstructor : any, func : any, message : string) : void {
    if (typeof func !== "function") {
        $ERROR ('assert.throws requires two arguments: the error constructor ' + 'and a function to run');
        return;
    }
    if (message === undefined) {
        message = '';
    } else {
        message += ' ';
    }
    try {
        func ();
    }
    catch (thrown : any) {
        if (typeof thrown !== 'object' || thrown === null) {
            message += 'Thrown value was not an object!';
            $ERROR (message);
        } else if (thrown.constructor !== expectedErrorConstructor) {
            message += 'Expected a ' + expectedErrorConstructor.name + ' but got a ' + thrown.constructor.name;
            $ERROR (message);
        }
        return;
    }
    message += 'Expected a ' + expectedErrorConstructor.name + ' to be thrown but no exception was thrown at all';
    $ERROR (message);
}
;
function Test262Error (this : JSDynamicObject, message : string) : void {
    this.message = message || "";
}
exports.Test262Error = Test262Error;
Test262Error.prototype.toString = function (this : JSDynamicObject) : string {
    return "Test262Error: " + this.message;
}
;
function $ERROR (message : string) : void {
    throw new Test262Error (message);
}
