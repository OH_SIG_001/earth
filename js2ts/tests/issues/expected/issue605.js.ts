class a {
    async getData () : Promise <string> {
        try {
            const response : any = await fetch ('https://jsonplaceholder.typicode.com/posts/1');
            const data : any = await response.json ();
            return JSON.stringify (data);
        }
        catch (error : any) {
            console.error (error);
            throw error;
        }
    }
}
var b : a = new a ();
console.log (b.getData ());
