function x (d : any) : any {
    return d.x + d.vx;
}
function y (d : any) : any {
    return d.y + d.vy;
}
function z (radius : number) : (x1 : any, x2 : any, x3 : any, x4 : any, x5 : any) => number {
    function apply (quad : any, x0 : any, y0 : any, x1 : any, y1 : any) : number {
        var data : any = quad.data;
        var node ! : any;
        if (data) {
            var x : any = x0;
            var y : any = y0;
            var l : any = x * x + y * y;
            if (x0 > 1) {
                node.vx += x *= l;
                node.vy += y *= l;
            }
        }
        return 1;
    }
    return apply;
}
console.log (z (42));
