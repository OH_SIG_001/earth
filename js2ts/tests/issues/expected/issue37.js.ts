class Circle {
    radius : any;
    draw : any;
    constructor (radius : any) {
        this.radius = radius;
        this.draw = function () : void {
            console.log ('draw circle');
        }
    }
}
const circle : Circle = new Circle (1);
circle.draw ();
class Rect {
    length : number;
    width : any;
    draw : any;
    constructor (length : any, width : any) {
        this.length = length;
        this.width = width;
        this.draw = function () : void {
            console.log ('draw rect');
        }
    }
}
const square : Rect = new Rect (4, 4);
square.draw ();
