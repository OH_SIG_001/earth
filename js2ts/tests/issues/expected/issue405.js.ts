var iterCount : number = 0;
async function * fn () : AsyncGenerator <any, any, any> {
    for await ({a = 3, b = 4} of [{}]) {
        iterCount += 1;
    }
}
var x : AsyncGenerator <any, any, any> = fn ();
console.log (x);
console.log (iterCount);
