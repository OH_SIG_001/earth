type JSDynamicObject = any;
const registry : FinalizationRegistry <any> = new FinalizationRegistry <any> ((key : any) : any => {
        console.log (`Finalizing object with key ${key}`);
    });
var obj : JSDynamicObject = {
    name : "John"
};
var ret : void = registry.register (obj, "myKey");
console.log (ret);
ret = registry.unregister (obj);
console.log (ret);
const weakRef : WeakRef <any> = new WeakRef <any> (obj);
ret = registry.register (weakRef, "myKey");
console.log (ret);
obj = null;
var dref : any = weakRef.deref ();
console.log (dref);
