type JSDynamicObject = any;
export default function (this : JSDynamicObject, ... JSarguments : any []) : any {
    var callback : any = JSarguments [0];
    JSarguments [0] = this;
    callback.apply (null, JSarguments);
    return this;
}
