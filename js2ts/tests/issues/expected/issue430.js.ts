type JSDynamicObject = any;
"use strict";
function SQL (parts : any, ... values : any) : JSDynamicObject {
    let str : string = "";
    let data : any [] = [];
    return new Fragment (str, data);
}
class Fragment {
    raw : string;
    values : any [];
    text : any;
    constructor (text : string, values : any []) {
        this.raw = text;
        this.values = values;
        let parts : string [] = text.split ('?:');
        this.text = parts.map (function (v : any, i : any) : any {
            return i == values.length ? v : `${v}$${i + 1}`;
        }).join ('');
    }
}
SQL.insert = function (table : any, values : any) : JSDynamicObject {
    let keys : string [] = Object.keys (values);
    return SQL `INSERT INTO $id${table} $keys${keys} $values${values}`;
}
;
