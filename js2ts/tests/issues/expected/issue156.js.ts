var callCount : number = 0;
var i ! : number;
function f ( ... JSarguments : any []) : void {
    for (i = 0; i < JSarguments.length; i ++) {
        callCount += JSarguments [i];
    }
}
;
f (5, ... ["six", "seven", "eight"]);
console.log (callCount);
