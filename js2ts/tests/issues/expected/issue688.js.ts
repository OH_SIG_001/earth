type JSDynamicObject = any;
var value1 : number = 123;
value1 ??= 1;
console.log (value1);
var value2 : bigint = 0n;
value2 ??= 1n;
console.log (value2);
class A {}
var value3 : number | A = 1;
value3 ??= new A ();
console.log (value3);
var obj : JSDynamicObject = {};
Object.defineProperty (obj, "prop", {
    value : 2,
    writable : true,
    enumerable : true,
    configurable : true
});
console.log (obj);
obj.prop ??= 1;
console.log (obj);
