export default class Monster {
    _lifePoints : number;
    _strength : number;
    constructor () {
        this._lifePoints = 85;
        this._strength = 63;
    }
    get lifePoints () : number {
        return this._lifePoints;
    }
    get strength () : number {
        return this._strength;
    }
    receiveDamage (attackPoints : any) : number {
        const damage : number = this.lifePoints - attackPoints;
        if (damage <= 0) {
            this._lifePoints = - 1;
            return this.lifePoints;
        }
        this._lifePoints = damage;
        return this.lifePoints;
    }
    attack (enemy : any) : void {
        enemy.receiveDamage (this._strength);
    }
}
