var a : string | number = 'abc';
function findMax ( ... JSarguments : number []) : string | number {
    if (JSarguments [0] > - Infinity) {
        a = JSarguments [0];
    }
    if (a == String (1)) {
        return a;
    }
    return JSarguments [0];
}
let result : string | number = findMax (1, 2, 3, 4, 5);
console.log (result);
