type JSDynamicObject = any;
var o2 : JSDynamicObject = {};
Object.defineProperty (o2, "bar", {
    get : function (this : JSDynamicObject) : number {
        this.barGetter = true;
        return 42;
    },
    set : function (this : JSDynamicObject, x : any) : void {
        this.barSetter = true;
    }
});
console.log (o2.bar);
