let a : number = 123;
console.log (++ a);
let b : bigint = 123n;
console.log (++ b);
let c : string = "123";
console.log (++ (c as any));
let d : number = 123;
console.log (++ d);
