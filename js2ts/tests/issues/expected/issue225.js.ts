var y : string | number = "abc";
console.log (y);
y = 123;
console.log (y);
var x : number = 40;
var m : number [] | number = [40, 41, 42];
console.log (m);
if (m [0] == x) {
    m = m [2];
}
console.log (m);
function f (x : number) : string {
    if (x == 42) {
        return "abc";
    }
    return "def";
}
var w : number | string = 42;
console.log (w);
w = f (42);
console.log (w);
