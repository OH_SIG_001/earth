function pad (value : any, width : number) : string {
    var s : string = value + "";
    var length : number = s.length;
    return length < width ? new Array (width - length + 1).join (0) + s : s;
}
function formatDate (date : any) : string {
    var hours : any = date.getUTCHours ();
    var minutes : any = date.getUTCMinutes ();
    var seconds : any = date.getUTCSeconds ();
    var milliseconds : any = date.getUTCMilliseconds ();
    return isNaN (date) ? "Invalid Date" : date.getUTCFullYear () + "-" + pad (date.getUTCMonth () + 1, 2) + "-" + pad (date.getUTCDate (), 2) + (milliseconds ? "T" + pad (hours, 2) + ":" + pad (minutes, 2) + ":" + pad (seconds, 2) + "." + pad (milliseconds, 3) + "Z" : seconds ? "T" + pad (hours, 2) + ":" + pad (minutes, 2) + ":" + pad (seconds, 2) + "Z" : minutes || hours ? "T" + pad (hours, 2) + ":" + pad (minutes, 2) + "Z" : "");
}
function formatValue (value : any) : any {
    return value == null ? "" : value instanceof Date ? formatDate (value) : value;
}
formatValue ("2023-07-27");
