type JSDynamicObject = any;
const testData : JSDynamicObject = {
    "primary" : "level1",
    "tertiary" : "level3",
};
function sameValue (v1 : number, v2 : number | boolean | string, message ? : string) : void {
    if (! (v1 === Number (v2))) console.log (message);
}
function assert (t1 : any, message : string) : void {
    if (! t1) console.log (message);
}
for (let [alias, name] of Object.entries (testData)) {
    let tag : string = "und-u-ks-" + alias;
    let canonical : string = "und-u-ks-" + name;
    sameValue (isCanonicalizedStructurallyValidLanguageTag (tag), false, "\"" + tag + "\" isn't a canonical language tag.");
    assert (isCanonicalizedStructurallyValidLanguageTag (canonical), "\"" + canonical + "\" is a canonical and structurally valid language tag.");
    let result : any [] = Intl.getCanonicalLocales (tag);
    sameValue (result.length, 1);
    sameValue (result [0], canonical);
}
