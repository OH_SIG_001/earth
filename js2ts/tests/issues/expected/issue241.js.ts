var a : number | string = 111;
{
    var a : number | string = "abc";
    console.log (a);
}
console.log (a);
function foo1 () : number {
    var a : number = 222;
    console.log (a);
    function bar () : number {
        return a;
    }
    return a;
}
console.log (foo1 ());
function foo2 () : number | string {
    var a : number | string = 333;
    console.log (a);
    if (Number (a) == 333) {
        var a : number | string = "def";
        console.log (a);
    }
    console.log (a);
    function bar () : number | string {
        return a;
    }
    return a;
}
console.log (foo2 ());
