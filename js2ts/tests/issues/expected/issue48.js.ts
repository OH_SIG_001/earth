type JSDynamicObject = any;
let myUserSet : WeakSet <any> = new WeakSet <any> ();
let john : JSDynamicObject = {
    name : "John"
};
let rocky : JSDynamicObject = {
    name : "Rocky"
};
let alex : JSDynamicObject = {
    name : "Alex"
};
let nick : JSDynamicObject = {
    name : "Nick"
};
myUserSet.add (john);
myUserSet.add (rocky);
myUserSet.add (john);
myUserSet.add (nick);
console.log (myUserSet.has (john));
console.log (myUserSet.has (alex));
console.log (myUserSet.delete (nick));
console.log (myUserSet.has (nick));
john = null;
