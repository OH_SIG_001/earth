class C {
    #o : number;
    #℘ : any;
    #ZW_‌_NJ : number;
    #ZW_‍_J : number;
    * m () : any {
        return 42;
    }
    #\u{6F} : any;
    #\u2118 : any;
    #ZW_\u200C_NJ : any;
    #ZW_\u200D_J : any;;
    o (value : number) : number {
        this.#o = value;
        return this.#o;
    }
    foo (value : any) : any {
        this.#℘ = value;
        return this.#℘;
    }
    ZW_‌_NJ (value : number) : number {
        this.#ZW_‌_NJ = value;
        return this.#ZW_‌_NJ;
    }
    ZW_‍_J (value : number) : number {
        this.#ZW_‍_J = value;
        return this.#ZW_‍_J;
    }
}
var c : C = new C ();
assert.sameValue (c.m ().next ().value, 42);
assert (! Object.prototype.hasOwnProperty.call (c, "m"), "m doesn't appear as an own property on the C instance");
assert.sameValue (c.m, C.prototype.m);
verifyProperty (C.prototype, "m", {
    enumerable : false,
    configurable : true,
    writable : true,
});
assert.sameValue (c.o (1), 1);
assert.sameValue (c.℘ (1), 1);
assert.sameValue (c.ZW_‌_NJ (1), 1);
assert.sameValue (c.ZW_‍_J (1), 1);
