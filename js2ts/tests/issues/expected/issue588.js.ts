type JSDynamicObject = any;
var parseTwoDigitYear : any = function (input : any) : any {
    return input;
}
;
var o : JSDynamicObject = {
    parseTwoDigitYear : function (input : number) : number {
        input = (+ input);
        return input + (input > 68 ? 1900 : 2000);
    }
};
if (o && o.parseTwoDigitYear) {
    parseTwoDigitYear = o.parseTwoDigitYear;
}
console.log (parseTwoDigitYear (1));
