type JSDynamicObject = any;
var F : JSDynamicObject = {};
var callCount : number = 0;
var thisValue ! : any;
var args ! : any [];
F [Symbol.hasInstance] = function (this : JSDynamicObject, ... JSarguments : any []) : void {
    thisValue = this;
    args = JSarguments;
    callCount += 1;
}
;
0 instanceof F;
console.log (callCount, 1);
console.log (thisValue, F);
console.log (args.length, 1);
console.log (args [0], 0);
