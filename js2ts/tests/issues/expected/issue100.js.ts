class Square {
    length : number | string;
    constructor (length : string | number) {
        this.length = length;
    }
    get perimeter () : number {
        return (this.length as any) + (this.length as any) + (this.length as any) + (this.length as any);
    }
    set perimeter (value : any) {
        this.length = value;
    }
}
let p1 : Square = new Square (123);
console.log (p1.perimeter);
let p2 : Square = new Square ("123");
console.log (p2.perimeter);
