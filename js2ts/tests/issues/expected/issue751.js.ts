assert.throws (RangeError, function () : void {
    (- 1n) ** BigInt (- 1n);
}, '(-1n) ** -1n throws RangeError');
