var foo : (x1 : any) => string = function (obj : any) : string {
    if (typeof obj === 'string') {
        foo = function foo (obj : any) : string {
            return typeof obj;
        }
    } else {
        foo = function foo (obj : any) : string {
            return "not string";
        }
    }
    return foo (obj);
}
console.log (foo ("a"));
console.log (foo (1));
