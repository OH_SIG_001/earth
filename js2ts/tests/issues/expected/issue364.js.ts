const func : Function = new Function ("a", "b", "return a + b;");
console.log (func (3, 4));
const exampleFunction : any = function example (a : any, b : any) : any {
    return a * b;
}
;
let _len : number = exampleFunction.length;
console.log (_len);
let _name : string = exampleFunction.name;
console.log (_name);
const proto : any = Object.getPrototypeOf (exampleFunction);
console.log (proto === Function.prototype);
var _apply : any = exampleFunction.apply (null, [3, 4]);
console.log (_apply);
const boundFunction : any = exampleFunction.bind (null, 5);
console.log (boundFunction (6));
let _call : any = exampleFunction.call (null, 3, 4);
console.log (_call);
var _tostring : string = exampleFunction.toString ();
console.log (_tostring);
