type JSDynamicObject = any;
var compiled ! : any;
function route (routes : string) : any {
    compiled = Object.keys (routes).map (function (route : string) : JSDynamicObject {
        if (route [0] !== "/") console.log ("Routes must start with a '/'.");
        if (/:([^\/\.-]+)(\.{3})?:/.test (route)) {
            console.log ("Route parameter names must be separated with either '/', '.', or '-'.");
        }
        return {
            route : route,
        };
    })
}
console.log (route ("/router.js"));
