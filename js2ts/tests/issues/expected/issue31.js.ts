class Square {
    length : number;
    constructor (length : number) {
        this.length = length;
    }
    get area () : number {
        return this.length + this.length;
    }
    set area (value : any) {
        this.area = value;
    }
}
let s : Square = new Square (123);
console.log (s.area);
