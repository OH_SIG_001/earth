type JSDynamicObject = any;
'use strict';
var doc : JSDynamicObject = {
    cookie : "x=4"
};
function read_cookie (name : string) : any [] {
    var result : any [] = doc.cookie.match (new RegExp (name + '=([^;]+)'));
    result = result != null ? JSON.parse (result [1]) : [];
    return result;
}
console.log (read_cookie ("x"));
