type JSDynamicObject = any;
function BasisOpen (this : JSDynamicObject, context : string) : void {
    this._context = context;
}
BasisOpen.prototype = {
    areaStart : function (this : JSDynamicObject) : void {
        this._line = 0;
    }
}
var o : any = new BasisOpen ("context");
