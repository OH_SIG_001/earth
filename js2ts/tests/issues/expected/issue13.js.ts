function * myGenerator (i : number) : Generator <number, any, any> {
    yield i + 10;
    yield i + 20;
    return i + 30;
}
const myGenObj : Generator <number, any, any> = myGenerator (10);
console.log (myGenObj.next ().value);
console.log (myGenObj.next ().value);
console.log (myGenObj.next ().value);
function * g () : Generator <string|number, any, any> {
    yield 123;
    yield "abc";
}
var iter : Generator <string|number, any, any> = g ();
var result1 ! : any;
var result2 ! : any;
result1 = iter.next ();
console.log (result1.value);
result2 = iter.next ();
console.log (result2.value);
