type JSNoValue = any;
function safeMin (x : number, y : number) : number | JSNoValue {
    if (x < y !== y < x) {
        return y < x ? y : x;
    }
    return undefined;
}
console.log (safeMin (1, 2));
