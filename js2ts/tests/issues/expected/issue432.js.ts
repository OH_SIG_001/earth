type JSDynamicObject = any;
function monster1 (this : JSDynamicObject, disposition : any) : void {
    this.disposition = disposition;
}
const handler1 : JSDynamicObject = {
    construct (target : any, args : any) : any {
        console.log (`Creating a ${target.name}`);
        console.log (typeof target);
        return new target ( ... args);
    }
};
const proxy1 : JSDynamicObject = new Proxy (monster1, handler1);
console.log (new proxy1 ('fierce').disposition);
