function resolveAfter2Seconds (x : number) : Promise <any> {
    return new Promise <any> ((resolve : any) : any => {
            setTimeout (() : any => {
                    resolve (x);
                }, 2000);
        });
}
async function add (x : number) : Promise <any> {
    const a : any = await resolveAfter2Seconds (20);
    const b : any = await resolveAfter2Seconds (30);
    return x + a + b;
}
add (10).then ((v : any) : any => {
        console.log (v);
    });
(async function (x : any) : Promise <any> {
    const p1 : Promise <any> = resolveAfter2Seconds (20);
    const p2 : Promise <any> = resolveAfter2Seconds (30);
    return x + await p1 + await p2;
} (10).then ((v : any) : any => {
        console.log (v);
    }));
