type JSDynamicObject = any;
var obj : JSDynamicObject = {
    a : 1,
    b : 2,
    c : 3
};
var sym : string | symbol = Symbol ('test');
console.log (sym);
for (sym in obj) {
    console.log (sym);
}
