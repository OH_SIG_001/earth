function assert_sameValue (v1 : number, v2 : number, m1 : string) : void {
    if (v1 != v2) {
        console.log ('Failed ' + m1);
    } else {
        console.log ('Succeeded ' + m1);
    }
}
var x : number = - 1;
assert_sameValue (x += - 1, - 2, 'U+0009 (expression)');
assert_sameValue (x, - 2, 'U+0009 (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+000B (expression)');
assert_sameValue (x, - 2, 'U+000B (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+000C (expression)');
assert_sameValue (x, - 2, 'U+000C (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+0020 (expression)');
assert_sameValue (x, - 2, 'U+0020 (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+00A0 (expression)');
assert_sameValue (x, - 2, 'U+00A0 (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+000A (expression)');
assert_sameValue (x, - 2, 'U+000A (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+000D (expression)');
assert_sameValue (x, - 2, 'U+000D (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+2028 (expression)');
assert_sameValue (x, - 2, 'U+2028 (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+2029 (expression)');
assert_sameValue (x, - 2, 'U+2029 (side effect)');
x = - 1;
assert_sameValue (x += - 1, - 2, 'U+0009U+000BU+000CU+0020U+00A0U+000AU+000DU+2028U+2029 (expression)');
assert_sameValue (x, - 2, 'U+0009U+000BU+000CU+0020U+00A0U+000AU+000DU+2028U+2029 (side effect)');
