assert.throws(TypeError, function() {
  ({
    [Symbol.toPrimitive]: function() {
      return Symbol('1');
    }
  }) ** 0n;
}, '({[Symbol.toPrimitive]: function() {return Symbol("1");}}) ** 0n throws TypeError');

assert.throws(TypeError, function() {
  0n ** {
    [Symbol.toPrimitive]: function() {
      return Symbol('1');
    }
  };
}, '0n ** {[Symbol.toPrimitive]: function() {return Symbol("1");}} throws TypeError');
