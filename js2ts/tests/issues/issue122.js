var x;
x = null ?? 123;
console.log(x)

var y;
y = undefined ?? "abc";
console.log(y)

var z;
var obj = {
    toString() {
        return null;
    },
    valueOf() {
        return null;
    }
};
z = obj ?? 1;
console.log(z)

var w;
w = 42 ?? 1;
console.log(w)


var u;
u = 42 ?? undefined;
console.log(u)