let box2d = {
    b2_toiCalls: 100,
    b2_toiIters: 200,
    b2_toiMaxIters: 50
};

let stats = `toi calls = ${box2d.b2_toiCalls.toFixed(0)}, ave toi iters = ${(box2d.b2_toiIters / box2d.b2_toiCalls).toFixed(1)}, max toi iters = ${box2d.b2_toiMaxIters.toFixed(0)}`;

console.log(stats);
