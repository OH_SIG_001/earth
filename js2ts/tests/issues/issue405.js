iterCount = 0;
async function* fn() {
    for await ({ a = 3, b = 4 } of [{}]) {
        iterCount += 1;
    }
}
x = fn();
console.log (x);
console.log (iterCount);
