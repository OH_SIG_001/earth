"use strict";

function SQL(parts, ...values) {
    let str = "", data = [];
    return new Fragment(str, data);
}

class Fragment {
    constructor(text, values) {
        this.raw = text;
        this.values = values;
        let parts = text.split('?:');
        this.text = parts.map(function (v, i) {
            return i == values.length ? v : `${v}$${i + 1}`;
        }).join('');

    }
}

SQL.insert = function (table, values) {
    let keys = Object.keys(values);
    return SQL`INSERT INTO $id${table} $keys${keys} $values${values}`;
};
