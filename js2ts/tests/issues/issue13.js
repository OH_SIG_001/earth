function * myGenerator(i) {
    yield i + 10;
    yield i + 20;
    return i + 30;
}

const myGenObj = myGenerator(10);

console.log(myGenObj.next().value); // 20
console.log(myGenObj.next().value); // 30
console.log(myGenObj.next().value); // 40

function * g() {
    yield 123;
    yield "abc";
}
var iter = g();
var result1;
var result2;
result1 = iter.next();
console.log(result1.value);
result2 = iter.next();
console.log(result2.value);
