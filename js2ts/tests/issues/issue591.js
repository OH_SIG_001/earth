const isNegative = number => number < 0
const absolute = number => Math.abs(number)
const getNumberUnitFormat = (number, unit) => {
  if (!number) {
    return {
      negative: false,
      format: ''
    }
  }

  if (isNegative(number)) {
    return {
      negative: true,
      format: `${absolute(number)}${unit}`
    }
  }

  return {
    negative: false,
    format: `${number}${unit}`
  }
}
console.log(getNumberUnitFormat(-10, 'cm'))
