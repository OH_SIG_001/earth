// #75
var await;
async function foo() {
    function bar() {
        await = 1;
    }
    bar();
}
foo();

async function callAsync() {
    await console.log(1);
    await console.log(2);
}
callAsync();
