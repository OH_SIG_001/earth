"use strict";
exports.__esModule = true;
exports.Test262Error = exports.assert = void 0;
function assert(mustBeTrue, message) {
    if (mustBeTrue === true) {
        return;
    }
    if (message === undefined) {
        message = 'Expected true but got ' + String(mustBeTrue);
    }
    $ERROR(message);
}
exports.assert = assert;
assert._isSameValue = function (a, b) {
    if (a === b) {
        return (a !== 0) || ((1 / a) === (1 / b));
    }
    return (a !== a) && (b !== b);
};
assert.sameValue = function (actual, expected, message) {
    if (assert._isSameValue(actual, expected)) {
        return;
    }
    if (message === undefined) {
        message = '';
    }
    else {
        message += ' ';
    }
    message += 'Expected SameValue(«' + String(actual) + '», «' + String(expected) + '») to be true';
    $ERROR(message);
};
assert.notSameValue = function (actual, unexpected, message) {
    if (!assert._isSameValue(actual, unexpected)) {
        return;
    }
    if (message === undefined) {
        message = '';
    }
    else {
        message += ' ';
    }
    message += 'Expected SameValue(«' + String(actual) + '», «' + String(unexpected) + '») to be false';
    $ERROR(message);
};
assert.throws = function (expectedErrorConstructor, func, message) {
    if (typeof func !== "function") {
        $ERROR('assert.throws requires two arguments: the error constructor ' + 'and a function to run');
        return;
    }
    if (message === undefined) {
        message = '';
    }
    else {
        message += ' ';
    }
    try {
        func();
    }
    catch (thrown) {
        if ((typeof thrown !== 'object') || (thrown === null)) {
            message += 'Thrown value was not an object!';
            $ERROR(message);
        }
        else if (thrown.constructor !== expectedErrorConstructor) {
            message += 'Expected a ' + expectedErrorConstructor.name + ' but got a ' + thrown.constructor.name;
            $ERROR(message);
        }
        return;
    }
    message += 'Expected a ' + expectedErrorConstructor.name + ' to be thrown but no exception was thrown at all';
    $ERROR(message);
};
function Test262Error(message) {
    this.message = message || "";
}
exports.Test262Error = Test262Error;
Test262Error.prototype.toString = function () {
    return "Test262Error: " + this.message;
};
var $ERROR = function $ERROR(message) {
    throw new Test262Error(message);
};
