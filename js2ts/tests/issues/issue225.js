// simple assignments
y = "abc";
console.log(y);
y = 123;
console.log(y);

// array assignments
x = 40;
m = [40,41,42];
console.log(m);
if (m[0] == x) {
    m = m[2];
}
console.log(m);

// function assignments
function f(x) { 
    if (x == 42) { 
        return ("abc"); 
    }
    return ("def");
}
w = 42;
console.log(w);
w = f (42);
console.log(w);
