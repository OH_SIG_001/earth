const a = new Number('123'); // a === 123 is false
const b = Number('123');     // b === 123 is true
console.log(typeof a); // "object" Number
console.log(typeof b); // "number"
