let myVal = 10
myVal++

console.log(myVal)

myVal--

console.log(myVal)

let myVal1 = '10'

console.log(+myVal1)

console.log(-myVal1)

console.log(!myVal)

console.log(~myVal)

let myFun = void function () {
    console.log('This is my function')
  }
  
console.log(myFun)

let myObj = {
    name: 'abc',
    age: 20,
  }
  
  delete myObj.name
  console.log(myObj)


  let myName = 'unknown'

console.log(typeof myName)