// Using a named property
const obj2 = {
    g: function* () {
      let index = 0;
      while (true) {
        yield index++;
      }
    },
  };
  const it1 = obj2.g();
  console.log(it1.next().value); // 0
  console.log(it1.next().value); // 1
  
  // The object using shorthand syntax
  const obj3 = {
    *g() {
      let index = 0;
      while (true) {
        yield index++;
      }
    },
  };
  
  const it = obj3.g();
  console.log(it.next().value); // 0
  console.log(it.next().value); // 1