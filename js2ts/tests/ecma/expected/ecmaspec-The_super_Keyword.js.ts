class Foo {
    name : string;
    constructor (name : string) {
        this.name = name;
    }
    getNameSeparator () : string {
        return '-';
    }
}
class FooBar extends Foo {
    index : number;
    constructor (name : string, index : number) {
        super (name);
        this.index = index;
    }
    getFullName () : string {
        return this.name + super.getNameSeparator () + this.index;
    }
}
const firstFooBar : FooBar = new FooBar ('foo', 1);
console.log (firstFooBar.name);
console.log (firstFooBar.getFullName ());
