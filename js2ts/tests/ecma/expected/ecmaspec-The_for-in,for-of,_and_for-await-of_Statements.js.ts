type JSDynamicObject = any;
async function * foo () : AsyncGenerator <number, any, any> {
    yield 1;
    yield 2;
}
(async function () : Promise <any> {
    for await (const num of foo ()) {
        console.log (num);
        break;
    }
} ());
const object : JSDynamicObject = {
    a : 1,
    b : 2,
    c : 3
};
for (const property in object) {
    console.log (`${property}: ${object[property]}`);
}
const array1 : string [] = ['a', 'b', 'c'];
for (const element of array1) {
    console.log (element);
}
