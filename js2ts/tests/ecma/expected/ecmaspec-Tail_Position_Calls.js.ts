function doA (a : number) : number {
    return a;
}
function doB (b : number) : number {
    return doA (b + 1);
}
function foo () : number {
    return 20 + doB (10);
}
console.log (foo ());
