class Car {
    make : string;
    model : string;
    year : number;
    constructor (make : string, model : string, year : number) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
}
const car1 : Car = new Car ('Eagle', 'Talon TSi', 1993);
console.log (car1.make);
