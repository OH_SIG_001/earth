"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.a2 = exports.a1 = void 0;
var c_1 = require("./lib2/c");
var b_1 = require("./b");
exports.a1 = 100;
function a2(n, s) {
    if (c_1.c2)
        return (0, b_1.b2)(n, s);
    else
        return (0, b_1.b2)(s, n);
}
exports.a2 = a2;
