## test_parser.sh 
测试 parser_tests 目录下的所有js文件
```
$ ./test_parser.sh ./parser_tests
```
可以在命令最后增加数字来明确线程数量，默认线程数为1

## testall.sh 
测试指定目录下的所有js文件。脚本会将目录下的所有js文件转化成.js.ts文件，再根据ts文件生成新的.js.js文件，最后测试文件是否可以运行
```
$ ./testall.sh DIR [txl options]
```

## transform.sh 
功能：针对整个js工程进行类型推理及转换  
将指定目录下的所有js文件转化成.ts文件，并根据jsconfig.json生成tsconfig.json   
该脚本文件需要build版本的两个执行文件js2ts和js2tsmf的支持，这两个执行文件可以直接从release区下载，也可以根据`../distrib/README.txt`说明自己构建
```
$ ./transform.sh directory-path
```
