#!/bin/sh
ulimit -s hard

# Check argument
if [[ ! "$1" =~ (.*\.js) ]]; then 
    echo "Usage:  test program.js [txl options]"
    exit 99
fi

# Clean up previous results
/bin/rm -f "$1".ts "$1".js

# Find JS files (only those named *.js)
txl -s 1000 -q -o "$1".ts "$1" js2ts.txl $2 $3 $4 $5
tsc -target es2021 --strict "$1".ts 
node "$1".js 

