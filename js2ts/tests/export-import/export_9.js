// Aggregating modules
//export * from "./export_9.js";
export * as variable1 from "./export_9.js";
//export { name1, /* …, */ nameN } from "./export_9.js";
//export { import1 as name1, import2 as name2, /* …, */ nameN } from "./export_9.js";
//export { default, /* …, */ } from "./export_9.js";
//export { default as name1 } from "./export_9.js";

export let name1 = "My name";
export let nameN = "My n'th name";