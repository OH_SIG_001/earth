function factorial (n : number, acc : number = 1) : number {
    if (n === 0) {
        return acc;
    }
    return factorial (n - 1, n * acc);
}
console.log (factorial (5));
console.log (factorial (10));
console.log (factorial (100));
console.log (factorial (1000));
