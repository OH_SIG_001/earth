const arr : string [] [] = [['a', '1'], ['b', '2'], ['c', '3']];
const obj : any = Object.fromEntries (arr);
console.log (obj);
const paramsString : string = 'param1=foo&param2=baz';
const searchParams : URLSearchParams = new URLSearchParams (paramsString);
Object.fromEntries (searchParams);
