const numberArray : any [] = [[1, 2], [[3], 4], [5, 6]];
const charArray : any [] = ['a',, 'b',,, ['c', 'd'], 'e'];
const flattenedArrOneLevel : any [] = numberArray.flat (1);
const flattenedArrTwoLevel : any [] = numberArray.flat (2);
const flattenedCharArrOneLevel : any [] = charArray.flat (1);
console.log (flattenedArrOneLevel);
console.log (flattenedArrTwoLevel);
console.log (flattenedCharArrOneLevel);
const numberArray1 : number [] [] = [[1], [2], [3], [4], [5]];
console.log (numberArray1.flatMap ((value : any) : number [] => {
        return [value * 10];
    }));
