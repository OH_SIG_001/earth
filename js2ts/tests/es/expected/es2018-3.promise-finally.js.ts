let isLoading : boolean = true;
fetch ('https://raw.githubusercontent.com/tonychunxizhu/j2test/master/people').then ((res : any) : any => {
        return res.json ();
    }).then ((json : any) : any => {
        return console.log (json);
    }).catch ((err : any) : any => {
        return console.error (err);
    }).finally (() : any => {
        isLoading = false;
        console.log ('Finished loading!!');
    })
