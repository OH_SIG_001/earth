function * myGenerator (i : number) : Generator <number, any, any> {
    yield i + 10;
    yield i + 20;
    return i + 30;
}
const myGenObj : Generator <number, any, any> = myGenerator (10);
console.log (myGenObj.next ().value);
console.log (myGenObj.next ().value);
console.log (myGenObj.next ().value);
