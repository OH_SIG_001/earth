class Square {
    length : number;
    constructor (length : any) {
        this.length = length;
    }
    get area () : number {
        return this.length * this.length;
    }
    set area (value : any) {
        this.area = value;
    }
}
class square {
    length : number;
    constructor (length : any) {
        this.length = length;
    }
    get area () : number {
        return this.length * this.length;
    }
    set area (value : any) {
        this.area = value;
    }
}
class Vehicle {
    name : string;
    constructor (name : any) {
        this.name = name;
    }
    start () : void {
        console.log (`${this.name} vehicle started`);
    }
}
class Car extends Vehicle {
    start () : void {
        console.log (`${this.name} car started`);
    }
}
const car : Car = new Car ('BMW');
console.log (car.start ());
