CPP_OUTPUT_FOLDER=target/djinni-example-rrr/cpp
ARKTS_OUTPUT_FOLDER=target/djinni-example-rrr/arkts
MY_PROJECT=target/djinni-example-rrr/proj-rrr.djinni


src/run --arkts-out $ARKTS_OUTPUT_FOLDER --arkts-type-prefix Node --arkts-module DjinniExampleRRR --arkts-include-cpp $CPP_OUTPUT_FOLDER --cpp-optional-header absl/types/optional.h --cpp-optional-template absl:optional --cpp-out $CPP_OUTPUT_FOLDER --idl $MY_PROJECT