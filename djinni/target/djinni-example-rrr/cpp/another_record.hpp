// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from proj-rrr.djinni

#ifndef DJINNI_GENERATED_ANOTHER_RECORD_HPP
#define DJINNI_GENERATED_ANOTHER_RECORD_HPP

#include <cstdint>
#include <iostream>
#include <string>
#include <utility>

struct AnotherRecord final {
    int32_t key1;
    std::string key2;

    friend bool operator==(const AnotherRecord& lhs, const AnotherRecord& rhs);
    friend bool operator!=(const AnotherRecord& lhs, const AnotherRecord& rhs);

    friend bool operator<(const AnotherRecord& lhs, const AnotherRecord& rhs);
    friend bool operator>(const AnotherRecord& lhs, const AnotherRecord& rhs);

    friend bool operator<=(const AnotherRecord& lhs, const AnotherRecord& rhs);
    friend bool operator>=(const AnotherRecord& lhs, const AnotherRecord& rhs);

    AnotherRecord(int32_t key1_,
                  std::string key2_)
    : key1(std::move(key1_))
    , key2(std::move(key2_))
    {}

    AnotherRecord(const AnotherRecord& cpy) {
       this->key1 = cpy.key1;
       this->key2 = cpy.key2;
    }

    AnotherRecord() = default;


    AnotherRecord& operator=(const AnotherRecord& cpy) {
       this->key1 = cpy.key1;
       this->key2 = cpy.key2;
       return *this;
    }

    template <class Archive>
    void load(Archive& archive) {
        archive(key1, key2);
    }

    template <class Archive>
    void save(Archive& archive) const {
        archive(key1, key2);
    }
};
#endif //DJINNI_GENERATED_ANOTHER_RECORD_HPP
