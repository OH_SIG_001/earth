// AUTOGENERATED FILE - DO NOT MODIFY!
// This file generated by Djinni from wchar_test.djinni

#ifndef DJINNI_GENERATED_WCHAR_TEST_HELPERS_HPP
#define DJINNI_GENERATED_WCHAR_TEST_HELPERS_HPP

#include <string>

namespace testsuite {

struct WcharTestRec;

class WcharTestHelpers {
public:
    virtual ~WcharTestHelpers() {}

    static WcharTestRec get_record();

    static std::wstring get_string();

    static bool check_string(const std::wstring & str);

    static bool check_record(const WcharTestRec & rec);
};

}  // namespace testsuite
#endif //DJINNI_GENERATED_WCHAR_TEST_HELPERS_HPP
