## 调用构造函数的入参要与标注类型匹配

由于TS语言类型系统是一种标注类型，不是编译期强制约束，如果入参的实际类型与标注类型不匹配，会影响引擎内部的优化效果。

【反例】

``` TypeScript
class A {
    private a: number | undefined;
    private b: number | undefined;
    private c: number | undefined;
    constructor(a?: number, b?: number, c?: number) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
// new的过程中没有传入参数，a，b，c会获取一个undefined的初值，和标注类型不符
let a = new A();
```

针对上文的示例场景，开发者大概率预期该入参类型是number类型，需要显式写出来。

参照正例进行如下修改，不然会造成标注的入参是number，实际传入的是undefined。

【正例】

``` TypeScript
class A {
    private a: number | undefined;
    private b: number | undefined;
    private c: number | undefined;
    constructor(a?: number, b?: number, c?: number) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
}
// 初始化直接传入默认值0
let a = new A(0, 0, 0);
```