## 不允许等待非Thenable的值

**【级别】规则**

**【描述】**

如果await一个非Thenable的值，await会把该值转换为已正常处理的Promise，然后等待其处理结果。此时await反而会影响代码性能。

**【反例】**

```javascript
async function f3() {
  const y = await 20;
  console.log(y); // 20
}

f3();
console.log(30);

// output
// 30
// 20
```

**【正例】**

```javascript
async function f3() {
  const y = 20;
  console.log(y); // 20
}

f3();
console.log(30);

// output
// 20
// 30
```