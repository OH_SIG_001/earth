在应用开发时，我们通常会使用log日志来调试代码。但是在正式版本中，大量的日志打印显然会
影响应用的性能。特别在某些高频操作中，大量的日志打印对应用的性能影响是致命的。  
因此，在正式发布版中，必须把这些日志打印代码删除或注释掉。
高频的操作一般包括
* onScroll
* onItemDragMove
* onTouch
* onDragMove
* onMouse
* onActionUpdate
* 组件属性绑定的函数

如下是一些案例说明
```
//sample.ets
import hilog from '@ohos.hilog';
import { logInfo } from '../../MyLog';

@Entry
@Component
struct Index {
    build() {
        Column() {
            Column() {               
                Scroll()
                    .onScroll(() => {
                    //反例
                    hilog.info(1001, 'Index', 'onScroll')
                    logInfo('info')
                    // do something
                })
            }
            Column() {               
                Scroll()
                    .onScroll(() => {
                    //正例
                    //hilog.info(1001, 'Index', 'onScroll')
                    //logInfo('info')
                    // do something
                })
            }
        }
    }
}

//MyLog.ts
import hilog from '@ohos.hilog'

export function logInfo(info){
hilog.info(1001,"MyLog",info);
}
```

onScroll
```
import hilog from '@ohos.hilog';

@Component
struct ExpOfOnScroll {
  build() {
    Stack() {
      // 反例
      Scroll()
        .onScroll(() => {
          hilog.info(1001, 'ExpOfOnScroll', 'some log')
          // do something
        })
      // 正例
      Scroll()
        .onScroll(() => {
          // do something
        })

    }
  }
}
```
onItemDragMove
```
@Component
struct ExpOfOnItemDragMove {
  build() {
    Stack() {
      // 反例
      Grid()
        .onItemDragMove(() => {
          hilog.info(1001, 'ExpOfOnItemDragMove', 'some log')
          // do something
        })
      // 正例
      Grid()
        .onItemDragMove(() => {
          // do something
        })

    }
  }
}
```
onTouch
```
@Component
struct ExpOfOnTouch {
  build() {
    Stack() {
      // 反例
      Grid()
        .onTouch(() => {
          hilog.info(1001, 'ExpOfOnTouch', 'some log')
          // do something
        })
      // 正例
      Grid()
        .onTouch(() => {
          // do something
        })
    }
  }
}
```
onDragMove
```
@Component
struct ExpOfOnDragMove {
  build() {
    Stack() {
      // 反例
      Grid()
        .onDragMove(() => {
          hilog.info(1001, 'ExpOfOnDragMove', 'some log')
          // do something
        })
      // 正例
      Grid()
        .onDragMove(() => {
          // do something
        })
    }
  }
}
```
onMouse
```
@Component
struct ExpOfOnMouse {
  build() {
    Stack() {
      // 反例
      Grid()
        .onMouse(() => {
          hilog.info(1001, 'ExpOfOnMouse', 'some log')
          // do something
        })
      // 正例
      Grid()
        .onMouse(() => {
          // do something
        })
    }
  }
}
```
onActionUpdate
```
@Component
struct ExpOfOnActionUpdate {
  build() {
    Stack() {
      // 反例
      Grid()
        .gesture(
          PanGesture()
            .onActionUpdate((event: GestureEvent|undefined) => {
              hilog.info(1001, 'ExpOfOnActionUpdate', 'some log')
              // do something
            })
        )

      // 正例
      Grid()
        .gesture(
          PanGesture()
            .onActionUpdate((event: GestureEvent|undefined) => {
              // do something
            })
        )
    }
  }
}
```

#### 在组件属性绑定的函数内部日志打印状态变量
反例
```
import hilog from '@ohos.hilog';
@Entry
@Component
struct Index {
    @State value1: number = 0;
    @State value2: number = 0;
    @State value3: number = 0;
    @State value4: number = 0;
    @State value5: number = 0;

    //反例    
    func() : number {
        hilog.info(1001, 'func', 'value1: ' + this.value1 + 'value2: ' + this.value2 +
        'value3: ' + this.value3 + 'value4: ' + this.value4 + 'value5: ' + this.value5);
        let result : number = 0;
        // do something
        hilog.info(1001, 'func', 'value1: ' + this.value1 + 'value2: ' + this.value2 +
        'value3: ' + this.value3 + 'value4: ' + this.value4 + 'value5: ' + this.value5);
        return result;
    }
build() {
    Column() {
          Column() {
            Text('text')
              .width(this.func())
          }
        }
    }
}
```
正例

```
import hilog from '@ohos.hilog';
@Entry
@Component
struct Index {
    @State value1: number = 0;
    @State value2: number = 0;
    @State value3: number = 0;
    @State value4: number = 0;
    @State value5: number = 0;

    //反例    
    func() : number {
        let result : number = 0;
        // do something
        return result;
    }
build() {
    Column() {
          Column() {
            Text('text')
              .width(this.func())
          }
        }
    }
}
```