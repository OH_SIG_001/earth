## 减少布局时间

减少布局时间可以通过异步加载和减少视图嵌套层次两种方法来实现。
#### 异步加载

同步加载的操作，使创建图像任务需要在主线程完成，页面布局Layout需要等待创建图像makePixelMap任务的执行，导致布局时间延长。相反，异步加载的操作，在其他线程完成，和页面布局Layout同时开始，且没有阻碍页面布局，所以页面布局更快，性能更好。但是，并不是所有的加载都必须使用异步加载，建议加载尺寸较小的本地图片时将syncLoad设为true，因为耗时较短，在主线程上执行即可。

**案例：使用Image组件同步加载高分辨率图片，阻塞UI线程，增加了页面布局总时间。**

```ts
@Entry
@Component
struct SyncLoadImage {
  @State arr: String[] = Array.from(Array<string>(100), (val,i) =>i.toString());
  build() {
    Column() {
      Row() {
        List() {
          ForEach(this.arr, (item: string) => {
            ListItem() {
              Image($r('app.media.4k'))
                .border({ width: 1 })
                .borderStyle(BorderStyle.Dashed)
                .height(100)
                .width(100)
                .syncLoad(true)
            }
          }, (item: string) => item.toString())
        }
      }
    }
  }
}
```

**优化：使用Image组件默认的异步加载方式加载图片，不阻塞UI线程，降低页面布局时间。**

```ts
@Entry
@Component
struct AsyncLoadImage {
  @State arr: String[] = Array.from(Array<string>(100), (val,i) =>i.toString());
    build() {
      Column() {
        Row() {
          List() {
            ForEach(this.arr, (item: string) => {
              ListItem() {
                Image($r('app.media.4k'))
                  .border({ width: 1 })
                  .borderStyle(BorderStyle.Dashed)
                  .height(100)
                  .width(100)
              }
            }, (item: string) => item.toString())
          }
        }
      }
  }
}
```