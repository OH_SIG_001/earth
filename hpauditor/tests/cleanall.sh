#!/bin/bash
# Remove old test runs
find . | grep -e '\.runout$' -e '\.parseout$' | xargs /bin/rm -f
/bin/rm -f tsaudit.x tsparsetest.x
