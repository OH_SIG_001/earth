/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue56.ts */
"use strict";
// The return value type is not declared void.
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue56.ts */
function test(): void {
  console.log('Hi');
}
// The return value type number is not declared.
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 7 : issue56.ts */
function fn(): number {
  return 1;
};
// The return value type is not declared as string.
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 12 : issue56.ts */
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 12 : issue56.ts */
const arrowFn: () => string = () => 'test';
class Test {
  
  // If there is no return value, the return value type is not declared void.
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 16 : issue56.ts */
  method(): void {
    console.log('Test');
  }
}
