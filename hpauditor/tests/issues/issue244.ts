
function addCardToDeskTopByHiboard(cardInfo: string): Promise<boolean> {
  let serviceAbility = new HiboardRemoteService(AbilityManager.getContext());
  return new Promise((resolve) => {
    serviceAbility.getProxy()
      .then((proxy: IdlHiboardServiceProxy) => {
        proxy.addCardToDeskTopByHiboard(cardInfo, (code: number, returnValue: boolean) => {
          log.showInfo(`addCardToDeskTopByHiboard, valur: ${returnValue}`);
          resolve(returnValue);
          serviceAbility.disConnect();
        });
      })
      .catch((err: Error) => {
        log.showError(`addCardToDeskTopByHiboard, err: ${err.message}`);
        serviceAbility.disConnect();
        return resolve(false);
      });
  });
}
