/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue01.ts */
"use strict";
// Do not omit 0s before and after the decimal point of a floating point number
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue01.ts */
/* HPAudit: Do not omit 0s before and after the decimal point : hp-specs-dec-num : 1 : 2 : issue01.ts */
const n1 = 0.1;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue01.ts */
/* HPAudit: Do not omit 0s before and after the decimal point : hp-specs-dec-num : 1 : 3 : issue01.ts */
const n2 = 1.0;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 4 : issue01.ts */
/* HPAudit: Do not omit 0s before and after the decimal point : hp-specs-dec-num : 1 : 4 : issue01.ts */
const n3 = 0.1e1;
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 5 : issue01.ts */
/* HPAudit: Do not omit 0s before and after the decimal point : hp-specs-dec-num : 1 : 5 : issue01.ts */
const n4 = 1.0e1;
