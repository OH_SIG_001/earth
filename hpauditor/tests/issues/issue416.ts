export default class BigFolderDragHandler {
  private appItemPosition = {
    positionX: [],
    positionY: []
  }

  public dragStart(dragItem: GridLayoutItemInfo, pageIndex: number, event: DragEvent, dragItemType: number, folderId: string): void {
    if (!dragItem) {
      log.showError('dragItem is null on dragStart');
      return;
    }
    Trace.start(Trace.CORE_METHOD_DRAG_START);
    log.showInfo('drag start, pageIndex: %{public}d dragItemType: %{public}d', pageIndex, dragItemType);
    AppStorage.setOrCreate<boolean>('isDrag', true);
    AppStorage.setOrCreate<number>('dragItemType', dragItemType);
    if (dragItem.itemType === undefined || dragItem.itemType === null) {
      dragItem.itemType = dragItem.typeId;
    }
    AppStorage.setOrCreate<GridLayoutItemInfo>('dragItemInfo', dragItem);
    DragEventManager.getInstance().dragStart(folderId, pageIndex);

    if (event) {
      const startPosition: DragItemPosition = {
        page: AppStorage.get<number>('pageIndex') as number,
        row: dragItem.row!,
        column: dragItem.column!,
        x: event.getWindowX(),
        y: event.getWindowY()
      };
      AppStorage.setOrCreate<Object>('startPosition', startPosition);
    }
    Trace.end(Trace.CORE_METHOD_DRAG_START);
  }

}
