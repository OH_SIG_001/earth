// Confusing equality operators
function sayHi(letter1, letter2) {
  let word = letter1 + letter2 === 'Hi' ? 'Hi!' : 'Not Hi';
  return word;
}

console.log(sayHi('H', 'i'));

function sayHi2(letter1, letter2) {
  let word = 'Hi' === letter1 + letter2 ? 'Hi!' : 'Not Hi';
  return word;
}

console.log(sayHi2('H', 'i'));
