let MAX_USER_COUNT: number = 10000;
let MIN_USER_COUNT: number = 10;
let overLimit: boolean = false;
let underLimit: boolean = true;
let userCount: number = 7;

// The line below exceeds line length limit.
if ((typeof userCount === "number") && (userCount > MAX_USER_COUNT && overLimit || userCount < MIN_USER_COUNT && underLimit)) {
  console.log('The number of users need to be managed!');
}
