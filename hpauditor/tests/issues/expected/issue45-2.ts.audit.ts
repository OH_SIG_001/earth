/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue45-2.ts */
"use strict";
// Confusing equality operators
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue45-2.ts */
function sayHi(letter1, letter2): boolean {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue45-2.ts */
  const word = letter1 + letter2 === 'Hi' ? 'Hi!' : 'Not Hi';
  return word;
}
console.log(sayHi('H', 'i'));
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 9 : issue45-2.ts */
function sayHi2(letter1, letter2): boolean {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 10 : issue45-2.ts */
  const word = 'Hi' === letter1 + letter2 ? 'Hi!' : 'Not Hi';
  return word;
}
console.log(sayHi2('H', 'i'));
