/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue418.ts */
"use strict";
export const EventConstants = { 

  // folder open component events
    EVENT_FOLDER_CLOSE_RENAME_INPUT: 'usual.event.EVENT_FOLDER_CLOSE_RENAME_INPUT',

  // gesture navigation events
    EVENT_NAVIGATOR_BAR_STATUS_CHANGE: 'usual.event.NAVIGATOR_BAR_STATUS_CHANGE',
    SYSTEM_BAR_CHANGE: 'usual.event.SYSTEM_BAR_CHANGE',
    EVENT_APP_DOCK_VISIBILITY_CHANGE: 'usual.event.APP_DOCK_VISIBILITY_CHANGE',
    EVENT_GESTURE_NAVIGATION_HIDE_TEXT_INPUT: 'usual.event.GESTURE_NAVIGATION_HIDE_TEXT_INPUT',

  // desktop icon menu event when long pressing
    EVENT_DESKTOP_MENU_EVENT: 'usual.event.DESKTOP_MENU_EVENT',

  // folder icon menu event when long pressing
    EVENT_FOLDER_MENU_EVENT: 'usual.event.FOLDER_MENU_EVENT',


  // form stack events
    EVENT_REQUEST_FORM_STACK_DELETE: 'launcher.event.EVENT_REQUEST_FORM_STACK_UPDATE',


  // systemUI panel status event
    EVENT_SYSTEMUI_PANEL_STATUS_CHANGED: 'COMMON_EVENT_SYSTEMUI_PANEL_STATUS_CHANGED',

  // float show events
    EVENT_FLOAT_ON_FOCUS: 'launcher.event.EVENT_FLOAT_ON_FOCUS',

  // Desktop or dock layout change event
    EVENT_LAYOUT_CHANGE_EVENT: 'launcher.event.LAYOUT_CHANGE',
    EVENT_INIT_DEFAULT_FILEFOLDER: 'INIT_DEFAULT_FILEFOLDER',

  // edit mode drop event
    EVENT_PAGEDESK_DROP_FINISHED: 'launcher.event.PAGEDESK_DROP_FINISHED',
  // edit mode item click
    EVENT_EDIT_MODE_TOUCH_DOWN_ITEM: 'launcher.event.EDIT_MODE_TOUCH_DOWN_ITEM',
    EVENT_FOLDER_DRAG_END: 'launcher.event.EVENT_FOLDER_DRAG_END',
  // small folder gray event
    EVENT_GRAY_PACKAGE_CHANGED: 'launcher.event.GRAY_PACKAGE_CHANGE',
    EVENT_UNINSTALL_ITEM_FROM_FOLDER: 'launcher.event.UNINSTALL_ITEM_FROM_FOLDER',
    EVENT_INIT_APPLIST_FINISH: 'launcher.event.init_applist_finish',

  // preview setting changed event
    EVENT_PREVIEW_STYLE_CHANGED: 'launcher.event.EVENT_PREVIEW_STYLE_CHANGED',

  // change desktop mode event
    EVENT_CHANGE_DESKTOP_MODE: 'launcher.event.CHANGE_DESKTOP_MODE'
  };
export default EventConstants;
