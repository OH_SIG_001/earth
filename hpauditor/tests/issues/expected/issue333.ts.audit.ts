/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue333.ts */
"use strict";
class Demo {
  private onAppListChange(): void {
    this.appGroupList = this.getGroup(this.appList, this.column);
  }
  onColumnChange(): void {
    this.appGroupList = this.getGroup(this.appList, this.column);
  }
  private getGroup(arr: AppItem[], n: number): AppItem[][] {
    n = n || 4;
    /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 12 : issue333.ts */
    const result: AppItem[][] = [];
    for (let i = 0; i < arr.length; i += n) {
      let itemArray = arr.slice(i, i + n);
      if (itemArray.length < n) {
        itemArray = itemArray.concat(new Array(n - itemArray.length).fill(undefined));
      }
      result.push(itemArray);
      if (this.maxLine != null && this.maxLine > 0 && result.length >= this.maxLine) {
        break;
      }
    }
    return result;
  }
}
