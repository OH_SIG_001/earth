/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue432.ts */
"use strict";
class Test {
  /* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue432.ts */
  protected getDragItemInfo() {
    /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 3 : issue432.ts */
    const dragItemInfo: any = AppStorage.Get('dragItemInfo');
    // avoid dragItemInfo from AppStorage is undefined
    return dragItemInfo ? dragItemInfo : {};
  }
}
