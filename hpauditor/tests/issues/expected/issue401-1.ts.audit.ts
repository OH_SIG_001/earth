/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue401-1.ts */
"use strict";
// What we can and can't detect and fix

// Some of these output NOT OK in the original TS code here
// Once fixed by TSaudit, they should all output OK
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 6 : issue401-1.ts */
const F1 = 0.21;
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 7 : issue401-1.ts */
const F2 = 0.1;
const f3 = F1 + F2;
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 9 : issue401-1.ts */
const F4 = 0.11;

// easy to detect: equality with float literal
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 12 : issue401-1.ts */
if (! (Math.abs((F1 - 0.1) - 0.11) < Number.EPSILON)) {
  console.log('NOT OK');
} else {
  console.log('OK');
}

// easy to detect: equality with float literal
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 19 : issue401-1.ts */
if (Math.abs((F1 - F2) - 0.11) < Number.EPSILON) {
  console.log('OK');
} else {
  console.log('NOT OK');
}

// can detect: equality with float initialized variable
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 26 : issue401-1.ts */
if (! (Math.abs((f3 - F2) - F1) < Number.EPSILON)) {
  console.log('NOT OK');
} else {
  console.log('OK');
}
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 32 : issue401-1.ts */
const I1 = 1;
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 33 : issue401-1.ts */
const I2 = 2;

// can detect: do not flag, integer comparison
if (I2 - I1 !== 1) {
  console.log('NOT OK');
} else {
  console.log('OK');
}

// can detect: should be flagged, float comparison
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 43 : issue401-1.ts */
if (Math.abs((I2 - I1) - 1.0) < Number.EPSILON) {
  console.log('OK');
} else {
  console.log('NOT OK');
}
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 49 : issue401-1.ts */
const A1 = 1;
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 50 : issue401-1.ts */
const A2 = 3;

// can detect: do not flag, integer comparison
if (A2 - A1 !== A1 + A1) {
  console.log('NOT OK');
} else {
  console.log('OK');
}
let a3 = 3;
a3 = 3.0;

// can detect: should be flagged, float comparison
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 63 : issue401-1.ts */
if (! (Math.abs((a3 - A1) - (A1 + A1)) < Number.EPSILON)) {
  console.log('NOT OK');
} else {
  console.log('OK');
}

// can detect: should be flagged, float comparison
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 70 : issue401-1.ts */
if (Math.abs(I1 / I2 - I1 / 2) < Number.EPSILON) {
  console.log('OK');
} else {
  console.log('NOT OK');
}
let a4 = I1 / I2; // 0.5
a4 = a4 * 3; // 1.5


// possibly can not detect (but not in general): should be flagged, float comparison
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 80 : issue401-1.ts */
if (! (Math.abs(a4 * 2 - A2) < Number.EPSILON)) {
  console.log('NOT OK');
} else {
  console.log('OK');
}

// However, in general we can't detect all of them, and some will seem too eager, 

// for example, this one will be flagged, even though the computation is integer
/* HPAudit: Do not use equality operators on floating point data : hp-specs-no-equality-ops-floating : 1 : 88 : issue401-1.ts */
if (! (Math.abs(I2 / 2 - I1) < Number.EPSILON)) {
  console.log('NOT OK');
} else {
  console.log('OK');
}
