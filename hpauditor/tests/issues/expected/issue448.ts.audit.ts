/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue448.ts */
"use strict";
export class RecentAppService {
  private context: Context;
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 3 : issue448.ts */
  public static instance: RecentAppService;
  private constructor(context: Context) {
    this.context = context;
  }
  getAppItemInfosByBundleNames(bundleNames: string[]): Promise<AppItemInfo[]> {
    if (! bundleNames || ! bundleNames.length) {
      return Promise.resolve<AppItemInfo[]>([]);
    }
    return Promise.all<AppItemInfo|undefined>(bundleNames.map<Promise<AppItemInfo|undefined>>(async (bundleName: string): Promise<AppItemInfo|undefined> => {
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 15 : issue448.ts */
      const isInstalled = await this.isAppInstalled(bundleName).catch( () => {
          return false;
        });
      if (! isInstalled) {
        return undefined;
      }
      return launcherAbilityManager.getAppInfoByBundleName(bundleName).catch<undefined>( (err: BusinessError): undefined => {
        LOGGER.error(RecentAppService.name, this.getAppItemInfosByBundleNames.name, err);
        return undefined;
      })
    })).then( (resList: (AppItemInfo | undefined)[]) => {
      /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 30 : issue448.ts */
      return resList.filter( (item: AppItemInfo | undefined) => item !== undefined).map( (item: AppItemInfo | undefined): AppItemInfo => {
        return item as AppItemInfo;
      });
    });
  }
}
