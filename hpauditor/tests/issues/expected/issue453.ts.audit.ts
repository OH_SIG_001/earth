/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue453.ts */
"use strict";
class FilePicker {
  public static async saveFile(dirPath: string): Promise<void> {
    if (isSaveFileSuccess) {
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 4 : issue453.ts */
      const bundleName = storage.get<string>('bundleName');
      for (let i = 0; i < savedFileUris.length; i++) {
        await FilePickerUtil.grantUriPermission(savedFileUris[i], bundleName, wantConstant.Flags.FLAG_AUTH_READ_URI_PERMISSION | wantConstant.Flags.FLAG_AUTH_WRITE_URI_PERMISSION |
            // @ts-ignore
        wantConstant.Flags.FLAG_AUTH_PERSISTABLE_URI_PERMISSION);
      }
      /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 12 : issue453.ts */
      const want = { 
          parameters: { 
            'pick_path_return': savedFileUris
          }
        };
      this.returnAbilityResult(want, 0);
    }
  }
}
