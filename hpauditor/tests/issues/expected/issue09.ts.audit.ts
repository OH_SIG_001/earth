/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue09.ts */
"use strict";
class O1 {
  /* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 2 : issue09.ts */
  x: string | null = "";
  y: string | undefined = "";
}
let obj: O1 = { 
    x: "",
    y: ""
  };
obj.x = "xxx";
obj.y = "yyy";
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 9 : issue09.ts */
obj.x = null;
