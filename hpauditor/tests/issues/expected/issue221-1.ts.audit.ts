/* Already has "use strict" - don't add another one */
"use strict";
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue221-1.ts */
/* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 3 : issue221-1.ts */
const X = 42;
