/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue386-2.ts */
"use strict";
function foo(): number {
  let left = 5;
  /* HPAudit: Do not assign variables in control conditions : hp-specs-no-vars-control-condition-expns : 1 : 3 : issue386-2.ts */
  left = 4
  const right = (left) ? left - 5 : 0
  return right
}
