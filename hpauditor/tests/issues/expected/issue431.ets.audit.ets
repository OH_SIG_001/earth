import { NtfListVm } from '../../vm/NtfListVm';
import { NotificationEntry } from '../../info/NotificationEntry';
import { NtfListScrollController } from '../../controller/NtfListScrollController';
import curves from '@ohos.curves';
import { NotificationListItem } from './NotificationListItem';
@Component
export struct NotificationListContent {
  @State ntfList: Array<NotificationEntry> = [];
  private ntfListVM: NtfListVm
  private listScroller: NtfListScrollController;
  @Link phoneNtfCenterClearAllOpacity: number;
  build() {
    Row() {
      List({ 
        space: this.ntfListVM?.getNtfListStyle()?.ntfItemSpace,
        scroller: this.listScroller?.getScroller()
      }) {
        /* HPAudit: Consider using LazyForEach for long lists : hp-arkui-load-on-demand : 1 : 18 : issue431.ets */
        ForEach(this.ntfList, (entry: NotificationEntry) => {
          ListItem() {
            NotificationListItem({ 
              ntfGroupEntry: entry,
              radius: this.radiusSyncState >= 0 ? this.borderRadiusMap?.get(entry.getGroupKey()) ?? 0 : 0
            })
          }
            .transition(TransitionEffect.asymmetric(TransitionEffect.OPACITY.animation({ 
              duration: 167,
              delay: 83,
              curve: curves.interpolatingSpring(0, 1, 228, 23)
            }).combine(TransitionEffect.opacity(0)).combine(TransitionEffect.scale({ 
              x: 0.6,
              y: 0.6
            })), TransitionEffect.IDENTITY))
        }, (entry: NotificationEntry): string => {
          return entry.getGroupKey();
        })
      }
    }
  }
}
