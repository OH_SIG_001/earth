/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue05.ts */
"use strict";
// Do not use float number as the value for textsize and coordinates
/* HPAudit: Do not use float number as the value for textsize and coordinates : hp-performance-no-float-number : 1 : 3 : issue05.ts */
{ 
  "command": "DrawTextBlob",
  "visible": true,
  "x": 75,
  "y": 48,
  "bounds": [- 23, - 45, 139, 11],
  "paint": { 
    "color": [255, 255, 255, 255]
  },
  "runs": [{ 
      "font": { 
        "subpixelText": true,
        "textSize": 42,
        "edging": "antialias",
        "hinting": "slight",
        "typeface": { 
          "data": "/data/0"
        }
      }
    }]
}
