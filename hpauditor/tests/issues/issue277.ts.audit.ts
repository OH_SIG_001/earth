/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue277.ts */
"use strict";
function getDay(): number {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue277.ts */
  /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 2 : issue277.ts */
  const A = 42
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue277.ts */
  /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 3 : issue277.ts */
  const B = 142857
  let c = 0
  /* HPAudit: Do not access a const property in a heavy loop : hp-performance-no-const-prop-in-heavy-loop : 1 : 5 : issue277.ts */
  const T1 = A + B;
  for (let index: number = 100; index > 0; index++) {
    c = T1
  }
  return c
}
