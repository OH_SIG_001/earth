// Enum
enum userType {
  TEACHER = 0,
  STUDENT = 1,
};

// Class
class user {
  username: string;
  usertype: userType;
  constructor(username: string) {
    this.username = username;
    this.usertype = userType.TEACHER;
  }

  sayHi(): void {
    console.log(`Hi, ${this.username}`);
  }
}

// Namespace
namespace databaseentity {
  export class player {
    constructor(public name: string) { }
  }

  export const newPlayer = new player("Tony Again");
}

let tony = new user("Tony");
tony.sayHi();

console.log (databaseentity.newPlayer.name);
