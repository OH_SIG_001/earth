/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue29.ts */
"use strict";
// This is an incorrect example because number of arguments and argument types
// do not match parameters for the function.
function add(a: number, b: number): number {
  return a + b;
}
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 7 : issue29.ts */
add(1, 2, 3);
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 8 : issue29.ts */
add(1);
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 9 : issue29.ts */
add("hello", "world");
