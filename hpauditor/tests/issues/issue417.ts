export class ComponentMethodManager<T extends object> {
  private methodMap: { [key: string]: Function | undefined } = {};

  // @ts-expect-error Ignore type compile error, functions is ok
  call<MethodT extends keyof T>(method: MethodT, ...args: Parameters<T[MethodT]>): ReturnType<T[MethodT]> {
    return this.methodMap[method as unknown as string]!(...args);
  }
}
