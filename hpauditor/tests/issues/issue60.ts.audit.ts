/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue60.ts */
"use strict";
function foo(arg1: string, arg2: number, arg3: string): void {
  console.log(arg1);
  console.log(arg2);
  console.log(arg3);
}
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 7 : issue60.ts */
const anyTyped = 1 as any;
foo(anyTyped, 1, 'a');
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 11 : issue60.ts */
const tuple1: [string, any, string] = ['a', anyTyped, 'b'];
foo( ... tuple1);
function bar(arg: Set<string>): void {
  const iter = arg.entries();
  for (const entry of iter) {
    console.log(entry);
  }
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 20 : issue60.ts */
/* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 20 : issue60.ts */
const newSet: Set<any> = new Set<any>();
newSet.add('Hi');
newSet.add('Bye');
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 23 : issue60.ts */
bar(newSet);
