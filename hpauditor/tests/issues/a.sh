for i in `find ${1} -type f | egrep -e '\.ts$' | egrep -v '\.audit\.ts$'` ; do
    echo "=== $i ===" 
    echo {"processor" : "HPAudit","severity" : 0,"rules" : ["%2",]} > b.json
    hpaudit $i -rules a.json
done
