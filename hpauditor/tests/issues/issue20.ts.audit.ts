/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue20.ts */
"use strict";
class A {
  private a: number | string;
  private b: number | string;
  private c: number | string;
  constructor(a: number, b: number, c: number) {
    this.a = a;
    this.b = b;
    this.c = c;
  }
}

// a, b, c are initialized as strings, does not match type annotation.
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 14 : issue20.ts */
/* HPAudit: Arguments must match function parameters : hp-performance-args-match-params : 1 : 14 : issue20.ts */
const a = new A('a', 'b', 'c');
console.log(a);
