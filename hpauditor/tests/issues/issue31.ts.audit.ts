/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue31.ts */
"use strict";
// Errors would not be thrown for the following examples
// 1. Assigning to undeclared variables.
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue31.ts */
const mistypedVariable;
mistypedVarible = 10; // ReferenceError



// 2. Failing to assign to object properties.

// 2.1 Assignment to a non-writable global.
undefined = 5; // TypeError
Infinity = 5; // TypeError


// 2.2 Assignment to non-writable property.
const obj1 = {};
Object.defineProperty(obj1, "x", { 
  value: 17,
  writable: false
});
obj1.x = 10; // TypeError


// 2.3 Assignment to a getter-only property.
const obj2 = { 
    get x () {
      return 17;
    }
  }
obj2.x = 10; // TypeError


// 2.4 Assignment to a new property on a non-extensible object.
const fixedObj = {};
Object.preventExtensions(fixedObj);
fixedObj.newProp = 'hello'; // TypeError



// 3. Failing to delete object properties.

// 3.1 Deleting a non-configurable.
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 33 : issue31.ts */
Object.prototype = null; // TypeError
/* HPAudit: Do not delete properties of an object : hp-performance-no-delete-objs-props : 1 : 34 : issue31.ts */
[].length = null; // TypeError


// 3.2 Deleting plain names.
let x;
/* HPAudit: Do not delete variables : hp-performance-no-delete-objs-props : 1 : 38 : issue31.ts */
x = null; // SyntaxError



// 4. Duplicate parameter names.
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 42 : issue31.ts */
function sumOf(num1, num1, num2): number {
  
  // SyntaxError
  return num1 + num1 + num2; // Wrong if code ran.
}
