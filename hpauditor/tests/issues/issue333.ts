class Demo {
  private onAppListChange(): void {
    this.appGroupList = this.getGroup(this.appList, this.column);
  }

  onColumnChange(): void {
    this.appGroupList = this.getGroup(this.appList, this.column);
  }

  private getGroup(arr: AppItem[], n: number): AppItem[][] {
    n = n || 4;
    let result: AppItem[][] = [];
    for (let i = 0; i < arr.length; i += n) {
      let itemArray = arr.slice(i, i + n);
      if (itemArray.length < n) {
        itemArray = itemArray.concat(new Array(n - itemArray.length).fill(undefined));
      }
      result.push(itemArray);
      if (this.maxLine != null && this.maxLine > 0 && result.length >= this.maxLine) {
        break;
      }
    }

    return result;
  }
}
