/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue284.ts */
"use strict";
// do not await return
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 2 : issue284.ts */
async function bar(): number {
  return 42;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 6 : issue284.ts */
function ding(): number {
  return 0;
}

// do not await return
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 11 : issue284.ts */
async function foo() {
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 12 : issue284.ts */
  return bar(); // fix this
}

// do not await return
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 16 : issue284.ts */
async function foo2() {
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 17 : issue284.ts */
  const baz = bar(); // fix this
  return baz;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 21 : issue284.ts */
async function foo3() {
  const baz = await bar(); // do not fix this
  
  // real code
  const blat = baz;
  /* HPAudit: Use strict equality operators : hp-specs-strict-equality-ops : 1 : 25 : issue284.ts */
  if (blat === baz) {
    
    // do something
  }
  // end real code
  return baz;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 32 : issue284.ts */
async function foo4() {
  let baz = ding();
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 34 : issue284.ts */
  baz = bar(); // fix this
  return baz;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 38 : issue284.ts */
async function foo5(): number {
  let baz = ding();
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 40 : issue284.ts */
  baz = bar(); // fix this
  return baz + 1;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 44 : issue284.ts */
async function foo6() {
  const baz = await bar(); // do not fix this
  if (false) {
    
    ///some real code here
  }
  return baz;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 52 : issue284.ts */
async function foo7() {
  let baz = ding();
  ///some real code here
  /* HPAudit: Do not use return await outside try catch : hp-specs-no-return-await : 1 : 55 : issue284.ts */
  baz = bar(); // fix this
  return baz;
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 59 : issue284.ts */
async function foo8() {
  let baz = ding();
  ///some real code here
  baz = await bar(); // do not fix this
  if (false) {
    
    ///some real code here
  }
  return baz;
}
