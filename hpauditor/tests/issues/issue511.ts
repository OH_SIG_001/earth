import { LogDomain, LogHelper } from '../utils/LogHelper';
import { Trace } from '../utils/Trace';
const TAG = 'GcController';
const log: LogHelper = LogHelper.getLogHelper(LogDomain.SCB, TAG);
export const DELAY_GC_TIMEOUT: number = 1000;
function triGc(reason: string): void {
  log.showInfo(`triGc -> start forceFullGC, reason:${reason}`);
  Trace.start(`${TAG}_triGc`);
}
