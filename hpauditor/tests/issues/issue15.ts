// This is an incorrect example because JS Object is used as a container to process Map logic.

class Demo {
  private getInfo(t1: string, t2: string): string {
    let info: { [k: string]: string } = {};
    this.setInfo(info);
    t1 = info[t2];
    return (t1 != null) ? t1 : '';
  }
  
  private setInfo(info: { [k: string]: string }) {
    info['T1'] = '76';
    info['T2'] = '91';
    info['T3'] = '12';
  }

  public Info(t1: string, t2: string) {
    return this.getInfo(t1, t2);
  }
}

let myDemo = new Demo();
console.log(myDemo.Info('T1', 'T2'));
