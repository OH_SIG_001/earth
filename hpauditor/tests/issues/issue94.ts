// ArkTS formatting

// No space before ()
function f () : void {  // f()
}
f ();                   // f()

// No space before []
let a : number [] = [1, 2, 3];  // number[]
a [1] = 42;                     // a[1]

// No space after :
const x : number = 1;   // x:
class C {
    y : 1;              // y:
}

// Space after { and before } in object literals
const o : {a:number,b:number} = {a:1,b:2};    // { a: number, b: number } ... { a: 1, b: 2 }
