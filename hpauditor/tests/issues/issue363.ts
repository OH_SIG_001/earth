/**
 * in any class
 */
class MyClass {
  /* Member semicolon case */
  private max=20; private min =10;
  public age = 0;public name='';

  foo(){
    /* Declaration comma case */
    let a, b, c = "test";
    let d = 1, e = 2;

    /* Declaration semicolon case */
    let c = "semis";let f = 1; let g = 2;

    /* Assignment comma case */
    this.max=50, this.min=20,this.age=20,this.name='bob'
    c = "hi", d=42;

    /* Assignment semicolon case */
    this.max=40; this.min=20;this.age=20;this.name='bob';
    b="42";f=42;g=77
  }
}

/* Declaration comma case */
let a, b, c = "test";
let d = 1, e = 2, myObj=new(MyClass);

/* Declaration semicolon case */
let f = 1; let g = 2;

/* Assignment comma case */
myObj.age=20,myObj.name='bob', c = "hi", d=42;

/* Assignment semicolon case */
myObj.age=20;myObj.name='bob'; b="42";f=42;g=77
