const contName: string = 'name';

let container: { [key: string]: string } = {
  aaa: 'bbb',
  ccc: 'ddd',
  [contName]: 'container',
  7: '8',
}

// Can be replaced with the constant equivalents, such as container.aaa
delete container['aaa'];

// Dynamic, difficult-to-reason-about lookups
delete container[contName];

console.log(container);
