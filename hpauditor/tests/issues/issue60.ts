function foo(arg1: string, arg2: number, arg3: string): void {
  console.log(arg1);
  console.log(arg2);
  console.log(arg3);
}

const anyTyped = 1 as any;

foo(anyTyped, 1, 'a');

const tuple1: [string, any, string] = ['a', anyTyped, 'b'];
foo(...tuple1);

function bar(arg: Set<string>): void {
  const iter = arg.entries();
  for (const entry of iter) {
    console.log(entry);
  }
}
let newSet: Set<any> = new Set<any>();
newSet.add('Hi');
newSet.add('Bye');
bar(newSet);
