/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue63.ts */
"use strict";
// Enum
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 2 : issue63.ts */
enum UserType { TEACHER = 0, STUDENT = 1, };

// Class
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 8 : issue63.ts */
class User {
  username: string;
  usertype: UserType;
  constructor(username: string) {
    this.username = username;
    this.usertype = UserType.TEACHER;
  }
  sayHi(): void {
    console.log(`Hi, ${this.username}`);
  }
}

// Namespace
/* HPAudit: Use UpperCamelCase for class, enum and namespace names : hp-specs-ucamelcase-cls-enums-ns : 1 : 22 : issue63.ts */
namespace Databaseentity {
  export class player {
    constructor(public name: string) {
    }
  }
  export const newPlayer = new player("Tony Again");
}
/* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 30 : issue63.ts */
const tony = new User("Tony");
tony.sayHi();
console.log(Databaseentity.newPlayer.name);
