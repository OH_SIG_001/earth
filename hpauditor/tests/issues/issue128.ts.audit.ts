/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue128.ts */
"use strict";
function addNum(a: number, b: number): void {
  
    // Nested function
  /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 3 : issue128.ts */
  const logToConsole = (message: string): void => {
      console.log(message);
    }
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 7 : issue128.ts */
  const result = a + b;

    // Invoke the nested function.
  logToConsole("Result is " + result);
}
addNum(5, 3);
