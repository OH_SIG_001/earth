class my_class{
  constructor(public my_property:string) {
  }
}
enum my_enum{
  VALUE1,
  VALUE2,
  VALUE3
}
namespace my_namespace{
  export class MyClass{}
}
class MYCLASS{
  constructor(public my_property:string) {
  }
}
enum MYENUM{
  VALUE1,
  VALUE2,
  VALUE3
}
namespace MYNAMESPACE{
  export class MYCLASS{}
}
