class Test {
  protected getDragItemInfo() {
    const dragItemInfo: any = AppStorage.Get('dragItemInfo');
    // avoid dragItemInfo from AppStorage is undefined
    return dragItemInfo ? dragItemInfo : {};
  }
}
