class Test {
    public async access(uri: string): Promise<boolean> {
        let existJudgment = await FileAccessHelper.clientManager.access(uri);
        return Promise.resolve(existJudgment);
    }
}
