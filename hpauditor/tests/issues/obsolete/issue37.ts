// Use isNaN() to determine if a variable is NaN
let foo: number = NaN;
if (foo == NaN) {
  console.log('foo is NaN!'); // Not outputted, incorrect
}
if (foo != NaN) {
  console.log('foo is not NaN! But it is!'); // Outputted, incorrect
}
