async function f3(): Promise<void> {
  const a: number = await 20;
  const b: string = await 'A';
  const c: null = await null;
  const d: undefined = await undefined;
  console.log(a);
  console.log(b); 
  console.log(c); 
  console.log(d); 
  a = await 20;
  b = await 'A';
  c = await null;
  d = await undefined;
  console.log(a);
  console.log(b); 
  console.log(c); 
  console.log(d); 
  const e = await a;
  const f = await b;
  const g = await c;
  const h = await d;
  console.log(e);
  console.log(f); 
  console.log(g); 
  console.log(h); 
}

f3();
console.log(30);
// output
// 30
// 20
// A
// null
// undefined
