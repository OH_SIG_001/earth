/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue588.ts */
"use strict";
// test.ts
/* HPAudit: ArkUI should only be used in .ets files : hp-arkui-file-type : 0 : 4 : issue588.ts */
@Reusable
@Component
struct ChildComponent {
  @State desc: string = '';
  @State sum: number = 0;
  aboutToReuse(params: Record<string, Object>): void {
    this.desc = params.desc as string;
    this.sum = params.sum as number;
  }
  build() {
    Column() {
      
           // business logic
    }
  }
}
/* HPAudit: ArkUI should only be used in .ets files : hp-arkui-file-type : 0 : 22 : issue588.ts */
@Entry
@Component
struct Reuse {
  private data: BasicDateSource = new BasicDateSource();
  aboutToAppear(): void {
    for (let index = 0; index < 20; index++) {
      this.data.pushData(index.toString())
    }
  }
}
