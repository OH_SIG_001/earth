console.log(eval({ a: 2 })); // outputs { a: 2 }
console.log(eval('"a" + 2')); // outputs 'a2' 
console.log(eval('{ a: 2 }')); // outputs 2 
console.log(eval('let value = 1 + 1;')); // outputs undefined
