/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue366.ts */
"use strict";
function getInfo(t1: string, t2: string): string {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 2 : issue366.ts */
  const info = {};
  setInfo(info);
  /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 4 : issue366.ts */
  t1 = info[t1]
  return (t1 != null) ? t1 : '';
}
/* HPAudit: Explicitly declare return types of functions and methods : hp-specs-explicit-return-types : 1 : 8 : issue366.ts */
function setInfo(info: {}): void {
  /* HPAudit: Do not use indexed containers as maps : hp-performance-proper-data-structures : 1 : 9 : issue366.ts */
  info['foo'] = 42;
}
console.log(getInfo('foo', 'bar'))
