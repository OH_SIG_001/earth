let age: number = 17;
let bee: number = 20;
if (age == bee) {
    console.log(age);
}
let foo: boolean = true;
if (foo == true) {
    console.log(foo);
}
let bananas: number = 2;
if (bananas != 1) {
    console.log(bananas);
}
let value: undefined = undefined;
if (value == undefined) {
    console.log(value);
}
if (typeof foo == 'undefined') {
    console.log(foo);
}
console.log('hello' != 'world');
console.log(0 == 0)
console.log(true == true);
