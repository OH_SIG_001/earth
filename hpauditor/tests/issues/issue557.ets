@Observed
class ClassA {
  public c: number = 0;

  constructor(c: number) {
    this.c = c;
  }
}

@Component
struct PropChild1 {
  // This one should be changed
  @Prop testNum: ClassA; // @Prop will make the variable deep copied

  build() {
    Text(`PropChild testNum ${this.testNum.c}`)
  }
}

@Component
struct PropChild2 {
  // This one should not be changed
  @Prop testNum: ClassA; // @Prop will make the variable deep copied

  build() {
    this.testNum = this.testNum;
    Text(`PropChild testNum ${this.testNum.c}`)
  }
}

@Entry
@Component
struct Parent {
  @State testNum: ClassA[] = [new ClassA(1)];

  build() {
    Column() {
      Text(`Parent testNum ${this.testNum[0].c}`)
        .onClick(() => {
          this.testNum[0].c += 1;
        })

      // PropChild did not change the value inside @Prop testNum: ClassA，so it is better to use @ObjectLink
      PropChild({ testNum: this.testNum[0] })
    }
  }
}
