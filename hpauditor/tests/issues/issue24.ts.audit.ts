/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue24.ts */
"use strict";
// Avoid using closures
// Pass as parameters instead

// Example 1
const arr: number[] = [1, 2, 3];
function foo(): number {
  /* HPAudit: Avoid using closures - consider using parameters instead : hp-performance-no-closures : 1 : 7 : issue24.ts */
  return arr[0] + arr[1];
}
console.log(foo());

// Example 2
function greet(): void {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 13 : issue24.ts */
  /* HPAudit: Name literal constants in uppercase, words separated by underscores : hp-specs-enum-ucase-uscore : 1 : 13 : issue24.ts */
  const GREETING: string = "Hi";
  /* HPAudit: Do not dynamically declare functions and classes : hp-performance-no-dynamic-cls-func : 1 : 14 : issue24.ts */
  const sayGreeting = (): void => {
      console.log(GREETING);
    }
  sayGreeting();
}
greet();
