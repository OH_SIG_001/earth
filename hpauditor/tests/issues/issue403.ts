function foo(): number {
  let left = 5;
  // NOT a control assignment
  let right = left == 4 ? left - 5 : 0;
  return right;
}

function foo2(): number {
  let left = 5;
  // NOT a control assignment
  let right = left = 4 ? left - 5 : 0;
  return right;
}

function foo3(): number {
  let left = 5;
  // YES a control assignment
  let right = (left = 4) ? left - 5 : 0;
  return right;
}

function foo4(): number {
  let left = 5;
  // YES a control assignment
  let right = left == (left = 4) ? left - 5 : 0;
  return right;
}
