/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue244.ts */
"use strict";
function addCardToDeskTopByHiboard(cardInfo: string): Promise<boolean> {
  /* HPAudit: Declare unchanged variables as const : hp-performance-unchanged-const-vars : 1 : 3 : issue244.ts */
  const serviceAbility = new HiboardRemoteService(AbilityManager.getContext());
  return new Promise( (resolve) => {
    serviceAbility.getProxy().then( (proxy: IdlHiboardServiceProxy) => {
      proxy.addCardToDeskTopByHiboard(cardInfo, (code: number, returnValue: boolean) => {
        log.showInfo(`addCardToDeskTopByHiboard, valur: ${returnValue}`);
        resolve(returnValue);
        serviceAbility.disConnect();
      });
    }).catch( (err: Error) => {
      log.showError(`addCardToDeskTopByHiboard, err: ${err.message}`);
      serviceAbility.disConnect();
      return resolve(false);
    });
  });
}
