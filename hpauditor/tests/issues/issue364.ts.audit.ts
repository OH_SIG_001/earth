/* HPAudit: Always use strict mode : hp-specs-use-strict-mode : 1 : 0 : issue364.ts */
"use strict";
/**
 * in class
 */
class MyClass {
  /* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 5 : issue364.ts */
  /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 5 : issue364.ts */
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 5 : issue364.ts */
  private ACCESSPRIVATEPROPERTIES: any; //反例
  /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 6 : issue364.ts */
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 6 : issue364.ts */
  private accessprivateproperties: any;
  /* HPAudit: Use lowerCamelCase for variable and function names : hp-specs-lcamelcase-vars-funcs-params : 1 : 7 : issue364.ts */
  /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 7 : issue364.ts */
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 7 : issue364.ts */
  private AccessPrivateProperties: any; //反例
  /* HPAudit: Avoid using 'any' : hp-specs-no-any : 1 : 8 : issue364.ts */
  /* HPAudit: Object properties should be initialized : hp-performance-initialize-obj-props : 1 : 8 : issue364.ts */
  private accessPrivateProperties: any; //正例
}
