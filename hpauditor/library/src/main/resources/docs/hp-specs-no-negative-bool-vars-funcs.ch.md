## 避免使用否定的布尔变量名，布尔型的局部变量或方法须加上表达是非意义的前缀。

**反例：**

```javascript
let isNoError = true;
let isNotFound = false;
function empty() {}
function next() {}
```

**正例：**

```javascript
let isError = false;
let isFound = true;
function isEmpty() {}
function hasNext() {}
```