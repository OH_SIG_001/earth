## 强制将Null和Undefined作为独立类型标注

Null和Undefined作为独立类型标注，可以提高代码的安全性，避免空指针异常。

**【反例】**

```javascript
let userName: string;
userName = 'hello';
userName = undefined;
```

**【正例】**

```javascript
let userName: string | undefined;
userName = 'hello';
userName = undefined;
```