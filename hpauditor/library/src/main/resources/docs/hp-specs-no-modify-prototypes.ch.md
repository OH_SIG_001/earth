## 不要修改内置对象的原型，或向原型添加方法


内置对象作为一套公共接口，具有约定俗成的行为方式，若修改其原型，可能破坏接口语义。因此，永远不要修改内置对象的原型，或向原型添加方法。

**【反例】**

```javascript
Array.prototype.indexOf = function () {
  return -1;
}
// 其它地方使用的时候
const arr = [1, 1, 1, 1, 1, 2, 1, 1, 1];
console.log(arr.indexOf(2)); // 输出-1
```
