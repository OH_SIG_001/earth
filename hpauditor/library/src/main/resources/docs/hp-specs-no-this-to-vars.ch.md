## 不要将This赋值给一个变量，约束This在一个Scope内使用


箭头函数提供了更简洁的语法，并且箭头函数中的this对象指向是不变的，this绑定到定义时所在的对象，有更好的代码可读性。而保存this引用的方式，容易让开发人员搞混。

**【反例】**

```javascript
function foo() {
  const self = this;
  return function() {
    console.log(self);
  };
}
```

**【正例】**

```javascript
function foo() {
  return () => {
    console.log(this);
  };
}
```