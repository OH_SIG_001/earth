## 规则2.4 条件语句和循环语句的实现必须使用大括号包裹，即使只有一条语句。

**反例：**

```javascript
if (condition)
  console.log('success');

for(let idx = 0; idx < 5; ++idx)
  console.log(idx);
```

**正例：**

```javascript
if (condition) {
  console.log('success');
}

for(let idx = 0; idx < 5; ++idx) {
  console.log(idx);
}
```