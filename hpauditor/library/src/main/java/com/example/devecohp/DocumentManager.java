package com.example.devecohp;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class manage information relevant to the docs we provide about the rules
 */
public class DocumentManager {
    public static final String CHINESE =new String(new byte[]{-28, -72, -83, -26, -106, -121},StandardCharsets.UTF_8);
    public static final String [] SUPPORTED_LANGUAGE =new String[]{"ch","en"};
    public static final Map<String,String> LANGUAGE_NAME = Map.of("ch", CHINESE,"en","English");
    public static ArrayList<String> getSupportedLanguage(String ruleId) {
        ArrayList<String> result = new ArrayList<>();
        for(String language: SUPPORTED_LANGUAGE){
            URL u = Auditor.class.getClassLoader().getResource("docs/" + ruleId + "."+language+".md");
            if (u != null) {
                result.add(LANGUAGE_NAME.get(language));
            }
        }
        return result;

    }

    /**
     *
     * @param ruleId the id of the rule
     * @param language the language for the document
     * @return the doc for the rule in the chosen language to display in the right panel of the tool window
     */
    public static String getRuleContent(String ruleId, String language){
        String k=null;
        for(String i: LANGUAGE_NAME.keySet()){
            if(LANGUAGE_NAME.get(i).equals(language)){
                k=i;
                break;
            }
        }
        StringBuilder mdContent = new StringBuilder();

        try {
            InputStream is = Auditor.class.getClassLoader().getResourceAsStream("docs/" + ruleId + "."+k+".md");

            if (is != null) {
                // Create a BufferedReader to read the file
                BufferedReader br = new BufferedReader(new InputStreamReader(is,"UTF-8"));
                // Read the file line by line
                String line;
                while ((line = br.readLine()) != null) {
                    mdContent.append(line).append("\n");
                }
                // Close the BufferedReader
                br.close();
            } else {
            }} catch (IOException e) {
            e.printStackTrace();
        }
        return mdContent.toString();
    }
}
