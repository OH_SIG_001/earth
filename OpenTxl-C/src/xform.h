// OpentTxl-C Version 11 transformer
// Copyright 2023, James R. Cordy and others

#ifdef TXLMAIN
// Initialization
extern void transformer (void);

// Apply main transformation rule
extern void transformer_applyMainRule (treePT originalInputParseTreeTP, treePT *transformedInputParseTreeTP);
#endif

extern int transformer_maxTotalValues;  
// 1-origin, [1 .. transformer_maxTotalValues]
extern array (treePT, transformer_valueTP);
extern int transformer_valueCount;      

// Name of rule currently being applied (for error messages and such)
extern int transformer_applyingRuleName;  
extern int transformer_callingRuleName;  

// Rule call activation records
struct transformer_ruleEnvironmentT {
    tokenT name;                        // name of the called rule
    treePT scopeTP;                     // scope of the call
    treePT resultTP;                    // partially resolved result of the call
    treePT newscopeTP;                  // partially resolved new scope after the call
    int valuesBase;                     // call bindings for formal parameters and local variables
    int depth;                          // dynamic depth of the call
    unsigned short parentrefs;          // used in predefined rule [.], [,], [^] optimizations
    bool hasExported;                   // export flag - has the call exported anything?
    struct ruleLocalsT *localsListAddr; // rule's symbol table for formal parameters and local variables
};

extern array (struct transformer_ruleEnvironmentT, transformer_callEnvironment);  // [0 .. maxCallDepth]
extern int transformer_callDepth;       // 0

#ifdef GARBAGE
// Treespace used by compiled TXL program - filled in by xform.applyMainRule
// We don't disturb the trees and kids of the compiled TXL program when recovering garbage
extern  treePT transformer_lastCompileTree;
extern  kidPT transformer_lastCompileKid;
#endif
