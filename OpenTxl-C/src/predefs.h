// OpentTxl-C Version 11 predefined built-in functions
// Copyright 2023, James R Cordy and others

// Used only in xform.c transformer

// Initialization
extern void predefs_initializePredefs (void);

// Apply a predefined function
extern void predefs_applyPredefinedFunction (const int ruleIndex, const struct transformer_ruleEnvironmentT *ruleEnvironment, 
    const treePT originalTP, treePT *resultTP, bool *matched);

