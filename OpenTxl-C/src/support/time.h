// OpentTxl-C Version 11 safe portable precise time stamps
// Copyright 2024, James R. Cordy and others

// Get precise time stamp
extern char * time_precisetime();
