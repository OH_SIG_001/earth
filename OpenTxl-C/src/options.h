// OpentTxl-C Version 11 options
// Copyright 2023, James R Cordy and others

// Version
#define version "OpenTxl-C v11.3.7 (29.5.24) (c) 2024 James R. Cordy and others"

// Phase
#define INITIALIZE 0
#define COMPILE 1
#define TRANSFORM 2

// TXL option flags
#define firstOption     0
#define parse_print_p   0
#define apply_print_p   1
#define attr_p          2
#define tree_print_p    3
#define rule_print_p    4
#define result_tree_print_p     5
#define boot_parse_p    6
#define verbose_p       7
#define quiet_p         8
#define compile_p       9
#define load_p          10
#define stack_print_p   11
#define grammar_print_p 12
#define raw_p           13
#define txl_p           14
#define sharing_p       15
#define comment_token_p 16
#define width_p         17
#define usage_p         18
#define tokens_p        19
#define size_p          20
#define indent_p        23
#define darren_p        25
#define analyze_p       26
#define pattern_print_p 27
#define upper_p         29
#define lower_p         30
#define charinput_p     31
#define notabnl_p       32
#define xmlout_p        33
#define newline_p       34
#define case_p          35
#define multiline_p     36
#define nlcomments_p    37
#define lastOption      37

#ifdef TXLMAIN
// Initialization
extern void options (void);
#endif

// Used in txl.c and errors.c
extern int phase;

// Used in txl.c main process and xform.c transformer
extern int exitcode;

// Used everywhere
extern bool options_option[lastOption + 1];

// Used in txl.c main process, xform.c transformer, xform_debug.h debugger, loadstor.c compiled application load/store
extern string options_txlSourceFileName;
extern string options_inputSourceFileName;
extern string options_txlCompiledFileName;
extern string options_outputSourceFileName;

// Used in unparse.h unparser and loadstor.c compiled application load/store
extern int options_outputLineLength;
extern int options_indentIncrement;

// Used in options.h and loadstor.c compiled application load/store
extern string options_txlLibrary;
extern string options_optionIdChars;
extern string options_optionSpChars;

// Used only in limits.h and loadstor.c compiled application load/store
extern int options_txlSize;
extern int options_transformSize;

// Used only in xform.c transformer - 1-origin [1 .. maxProgramArguments]
extern string options_progArgs[maxProgramArguments + 1];
extern int options_nProgArgs;

// Used only in scan.c scanner - 1-origin [1 .. maxTxlIncludeLibs]
extern string options_txlIncludeLibs[maxTxlIncludeLibs + 1];
extern int options_nTxlIncludeLibs;
extern bool options_updatedChars;
extern void options_setIfdefSymbol (const string symbol);
extern int options_lookupIfdefSymbol (const string symbol);
extern void options_unsetIfdefSymbol (const string symbol);

// Used in scan.c scanner and xform_predefs.h built-in functions
extern void options_processOptionsString (const string optionsString);
