// OpentTxl-C Version 11 grammar compiler
// Copyright 2023, James R Cordy and others

#ifdef TXLMAIN
// Initialization in txl.c main process
extern void defineCompiler(void);

// Used only in txl.c main process
extern void defineCompiler_makeGrammarTree (const treePT txlParseTreeTP, treePT *inputGrammarTreeTP);
#endif
