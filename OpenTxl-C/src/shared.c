// OpentTxl-C Version 11 pre-hashed tokens and names
// J.R. Cordy, Jan 2023

// Copyright 2023, James R. Cordy and others

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
// and associated documentation files (the “Software”), to deal in the Software without restriction, 
// including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
// and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies 
// or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
// AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// OpenTxl pre-hashed tokens and names.
// Common names and tokens frequently used by the TXL processor are pre-hashed into the ident table 
// and globally named here to avoid repeated lookups.

// Modification Log

// v11.0 Initial revision, adapted from OpenTxl 11.0

// I/O, strings, memory allocation
#include "support.h"

// Global modules
#include "tokens.h"
#include "trees.h"
#include "idents.h"

// Check interface consistency
#include "shared.h"

// Shared pre-hashed global common names and trees

// empty
tokenT empty_T;
treePT emptyTP;

// comma, dot
tokenT comma_T, dot_T;
treePT commaTP;

// anonymous variable _
tokenT anonymous_T;

// formatting cues
tokenT NL_T, FL_T, IN_T, EX_T, SP_T, TAB_T, SPOFF_T, SPON_T;

// attributes (internal, implements [attr])
tokenT ATTR_T;

// parsing fences
tokenT KEEP_T, FENCE_T;

// lookaheads (internal, implement [see] and [not])
tokenT SEE_T, NOT_T;

// any, the polymorphic type
tokenT any_T;

// TXL language tokens
tokenT quote_T, underscore_T, openbracket_T, include_T, compounds_T, comments_T, keys_T, tokens_T, 
    end_T, redefine_T, star_T, dollar_T, dotDotDot_T, bar_T, quit_T, assert_T, ignore_T, each_T, match_T, replace_T;

// internal TXL name, helps resolve unresolvable TXL parse ambiguity
tokenT TXL_optBar_T;

// TXL built-in type names, visible to the user
tokenT stringlit_T, charlit_T, token_T, key_T, number_T, floatnumber_T, decimalnumber_T, integernumber_T, id_T, 
    comment_T, upperlowerid_T, upperid_T, lowerupperid_T, lowerid_T, space_T, newline_T, srclinenumber_T, srcfilename_T;

// TXL internal type names, not visible to the user
tokenT order_T, choose_T, literal_T, firstTime_T, subsequentUse_T, expression_T, lastExpression_T, ruleCall_T, leftchoose_T, 
    generaterepeat_T, repeat_T, generatelist_T, list_T, lookahead_T, push_T, pop_T, undefined_T;

//TXL built-in globals, visible to the user
tokenT TXLargs_T, TXLprogram_T, TXLinput_T, TXLexitcode_T;

// Tree kind name maps - declared in tokens.i
tokenT kindType[lastTreeKind + 1];

// Inverse map - type kind name to tree type
enum treeKindT typeKind (tokenT name)
{
    for (int k = firstTreeKind; k <= lastTreeKind; k++) {
        if (kindType[(enum treeKindT) k] == name) 
            return ((enum treeKindT) k);
    }
    return (treeKind_undefined);
}

// Blank string for indenting
string BLANKS;

// Initialization
void shared (void) {
    // empty
    empty_T = ident_install ("empty", treeKind_empty);
    emptyTP = tree_newTreeInit (treeKind_empty, empty_T, empty_T, 0, nilKid);

    // comma, dot
    comma_T = ident_install (",", treeKind_literal);
    commaTP = tree_newTreeInit (treeKind_literal, comma_T, comma_T, 0, nilKid);
    dot_T = ident_install (".", treeKind_literal);

    // the anonymous variable _
    anonymous_T = ident_install ("_", treeKind_id);

    // formatting cues
    NL_T = ident_install ("NL", treeKind_id);
    FL_T = ident_install ("FL", treeKind_id);
    IN_T = ident_install ("IN", treeKind_id);
    EX_T = ident_install ("EX", treeKind_id);
    SP_T = ident_install ("SP", treeKind_id);
    TAB_T = ident_install ("TAB", treeKind_id);
    SPOFF_T = ident_install ("SPOFF", treeKind_id);
    SPON_T = ident_install ("SPON", treeKind_id);

    // internal representation of [attr]
    ATTR_T = ident_install ("TXL_ATTR_", treeKind_id);

    // parsing fences
    KEEP_T = ident_install ("KEEP", treeKind_id);
    FENCE_T = ident_install ("FENCE", treeKind_id);

    // internal representation of lookaheads
    SEE_T = ident_install ("TXL_SEE_", treeKind_id);
    NOT_T = ident_install ("TXL_NOT_", treeKind_id);

    // the polymorphic type [any]
    any_T = ident_install ("any", treeKind_id);

    // TXL language tokens
    quote_T = ident_install ("\'", treeKind_literal);
    underscore_T = ident_install ("_", treeKind_id);
    openbracket_T = ident_install ("[", treeKind_literal);
    include_T = ident_install ("include", treeKind_id);
    compounds_T = ident_install ("compounds", treeKind_id);
    comments_T = ident_install ("comments", treeKind_id);
    keys_T = ident_install ("keys", treeKind_id);
    tokens_T = ident_install ("tokens", treeKind_id);
    end_T = ident_install ("end", treeKind_id);
    redefine_T = ident_install ("redefine", treeKind_id);
    star_T = ident_install ("*", treeKind_literal);
    dollar_T = ident_install ("$", treeKind_literal);
    dotDotDot_T = ident_install ("...", treeKind_literal);
    bar_T = ident_install ("|", treeKind_literal);
    quit_T = ident_install ("quit", treeKind_id);
    assert_T = ident_install ("assert", treeKind_id);
    ignore_T = ident_install ("ignore", treeKind_id);
    each_T = ident_install ("each", treeKind_id);
    match_T = ident_install ("match", treeKind_id);
    replace_T = ident_install ("replace", treeKind_id);

    // internal TXL name, assists in TXL parse ambiguity
    TXL_optBar_T = ident_install ("TXL_optBar_", treeKind_literal);

    // TXL built-in type names, visible to the user
    stringlit_T = ident_install ("stringlit", treeKind_id);
    charlit_T = ident_install ("charlit", treeKind_id);
    token_T = ident_install ("token", treeKind_id);
    key_T = ident_install ("key", treeKind_id);
    number_T = ident_install ("number", treeKind_id);
    floatnumber_T = ident_install ("floatnumber", treeKind_id);
    decimalnumber_T = ident_install ("decimalnumber", treeKind_id);
    integernumber_T = ident_install ("integernumber", treeKind_id);
    id_T = ident_install ("id", treeKind_id);
    comment_T = ident_install ("comment", treeKind_id);
    upperlowerid_T = ident_install ("upperlowerid", treeKind_id);
    upperid_T = ident_install ("upperid", treeKind_id);
    lowerupperid_T = ident_install ("lowerupperid", treeKind_id);
    lowerid_T = ident_install ("lowerid", treeKind_id);
    space_T = ident_install ("space", treeKind_id);
    newline_T = ident_install ("newline", treeKind_id);
    srclinenumber_T = ident_install ("srclinenumber", treeKind_id);
    srcfilename_T = ident_install ("srcfilename", treeKind_id);

    // internal type names, not visible to user
    order_T = ident_install ("*order", treeKind_id);
    choose_T = ident_install ("*choose", treeKind_id);
    literal_T = ident_install ("*literal", treeKind_id);
    firstTime_T = ident_install ("*firstTime", treeKind_id);
    subsequentUse_T = ident_install ("*subsequentUse", treeKind_id);
    expression_T = ident_install ("*expression", treeKind_id);
    lastExpression_T = ident_install ("*lastExpression", treeKind_id);
    ruleCall_T = ident_install ("*ruleCall", treeKind_id);
    leftchoose_T = ident_install ("*leftchoose", treeKind_id);
    generaterepeat_T = ident_install ("*generaterepeat", treeKind_id);
    repeat_T = ident_install ("*repeat", treeKind_id);
    generatelist_T = ident_install ("*generatelist", treeKind_id);
    list_T = ident_install ("*list", treeKind_id);
    lookahead_T = ident_install ("*lookahead", treeKind_id);
    push_T = ident_install ("*push", treeKind_id);
    pop_T = ident_install ("*pop", treeKind_id);
    undefined_T = ident_install ("*undefined", treeKind_id);

    // built-in global variables, visible to user
    TXLargs_T = ident_install ("TXLargs", treeKind_id);
    TXLprogram_T = ident_install ("TXLprogram", treeKind_id);
    TXLinput_T = ident_install ("TXLinput", treeKind_id);
    TXLexitcode_T = ident_install ("TXLexitcode", treeKind_id);

    // Tree kind name maps
    // structuring trees - order, choose, repeat, list
    kindType[treeKind_order] = order_T;
    kindType[treeKind_choose] = choose_T;
    kindType[treeKind_repeat] = repeat_T;
    kindType[treeKind_list] = list_T;

    // structure generator trees - leftchoose, generaterepeat, generatelist, lookahead, push, pop
    kindType[treeKind_leftchoose] = leftchoose_T;
    kindType[treeKind_generaterepeat] = generaterepeat_T;
    kindType[treeKind_generatelist] = generatelist_T;
    kindType[treeKind_lookahead] = lookahead_T;
    kindType[treeKind_push] = push_T;
    kindType[treeKind_pop] = pop_T;

    // the empty tree
    kindType[treeKind_empty] = empty_T;
    
    // leaf trees - literal, stringlit, charlit, token, id, upperlowerid, upperid, 
    // lowerupperid, lowerid, number, floatnumber, decimalnumber, 
    // integernumber, comment, key, space, newline, srclinenumber, srcfilename
    kindType[treeKind_literal] = literal_T;
    kindType[treeKind_stringlit] = stringlit_T;
    kindType[treeKind_charlit] = charlit_T;
    kindType[treeKind_token] = token_T;
    kindType[treeKind_id] = id_T;
    kindType[treeKind_upperlowerid] = upperlowerid_T;
    kindType[treeKind_upperid] = upperid_T;
    kindType[treeKind_lowerupperid] = lowerupperid_T;
    kindType[treeKind_lowerid] = lowerid_T;
    kindType[treeKind_number] = number_T;
    kindType[treeKind_floatnumber] = floatnumber_T;
    kindType[treeKind_decimalnumber] = decimalnumber_T;
    kindType[treeKind_integernumber] = integernumber_T;
    kindType[treeKind_comment] = comment_T;
    kindType[treeKind_key] = key_T;
    kindType[treeKind_space] = space_T;
    kindType[treeKind_newline] = newline_T;
    kindType[treeKind_srclinenumber] = srclinenumber_T;
    kindType[treeKind_srcfilename] = srcfilename_T;

    // user specified leaves - actual names set by scanner
    for (int ut = treeKind_usertoken1; ut <= treeKind_usertoken30; ut++) {
        kindType[(enum treeKindT) ut] = undefined_T;
    }

    // special trees - firstTime, subsequentUse, expression, lastExpression, ruleCall, undefined
    kindType[treeKind_firstTime] = firstTime_T;
    kindType[treeKind_subsequentUse] = subsequentUse_T;
    kindType[treeKind_expression] = expression_T;
    kindType[treeKind_lastExpression] = lastExpression_T;
    kindType[treeKind_ruleCall] = ruleCall_T;
    kindType[treeKind_undefined] = undefined_T;

    // Preformatted blanks for indenting
    stringrep (BLANKS, " ", maxStringLength);
}
