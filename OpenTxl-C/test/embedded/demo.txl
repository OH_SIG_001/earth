% Example embeddable TXL program
define program
    [repeat number]
end define

function main
    replace [program]
    	Numbers [repeat number]
    construct Sum [number]
    	_ [+ each Numbers]
    by
    	Sum
end function
