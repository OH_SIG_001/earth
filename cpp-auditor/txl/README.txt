Beta TXL Basis Grammar for C++20 with Macros
Version 0.7, August 2023

Copyright 2023 James R. Cordy
Licensed under the MIT open source license, see source for details.

Description:
    TXL basis grammar for C++20, designed for large scale C++ analysis tasks.  
    Validated on a range of popular open source C++ software including bitcoin, 
    mongo, opencv, bit torrent, opencv, serenity and others.

    Handles both preprocessed and unpreprocessed C code with with expanded or
    unexpanded C macro calls.  

    Optionally handles but does not interpret C preprocesor directives, 
    except #ifdefs that violate syntactic boundaries.  #ifdefs can be handled 
    using the separate Antoniol et al. transformation that keeps only the #else parts
    and comments out the optional (#if, #elsif) parts (ifdef.txl).

    Ignores and does not preserve comments. 

Authors:
    J.R. Cordy, Huawei Technologies

Example:
    txl program.cpp cpp.txl
    txl program.cpp ifdef.txl > program_ifdef.cpp;  txl program_ifdef.cpp cpp.txl

Notes:
    1. The syntax of the C++ language is not context-free, and there will always be
    cases that cannot be accurately parsed by a context-free grammar. 

    2. While this grammar handles many unexpanded Gnu/Linux-style macro calls,
    it cannot do so for all cases since macros may hide additional syntax.

Rev. 9.8.23
