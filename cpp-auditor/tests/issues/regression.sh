#!/bin/bash
# Usage: regression.sh [-comment] [other txl options]

# Clean up old results
/bin/rm -f *.cpp.audit.cpp

# Run each issue
for i in issue*.cpp
do
    echo "=== $i ==="
    txl $1 $2 $3 $4 -q $i cppaudit.txl >& $i.audit.cpp
    diff $i.audit.cpp expected/$i.audit.cpp
done
echo "=== ==="
