void foo (Blat * bar) {
    char * blat;
    int * ding = nullptr;
    blat = f (x);
    if (bar != nullptr) {
        * bar = blat;
        printf (UNCHECKED_POINTER_DEREFERENCE (* blat));
    }
    UNCHECKED_POINTER_DEREFERENCE (bar -> x) = 99;
    if (UNCHECKED_POINTER_DEREFERENCE (* ding) == 0) {
        UNCHECKED_POINTER_DEREFERENCE (bar -> x) = 42;
    }
    f (UNCHECKED_POINTER_DEREFERENCE (bah [x].z -> y));
}

void foo2 (Blat * bar) {
    char * blat;
    int * ding = nullptr;
    blat = f (x);
    if (bar != nullptr) {
        * bar = blat;
        printf (UNCHECKED_POINTER_DEREFERENCE (* blat));
    }
    UNCHECKED_POINTER_DEREFERENCE (bar -> x) = 99;
    if (UNCHECKED_POINTER_DEREFERENCE (* ding) == 0) {
        UNCHECKED_POINTER_DEREFERENCE (bar -> x) = 42;
    }
    CHECK_NULL_VOID (bah [x].z);
    f (bah [x].z -> y);
    if (blat != nullptr) {
        UNCHECKED_POINTER_DEREFERENCE (* bar) = blat;
        printf (* blat);
    }
    if ((bar != nullptr) && (blat != nullptr)) {
        * bar = blat;
        printf (* blat);
    }
    if (bar == nullptr) {
        flagerror ();
    }
    else {
        noerror (* bar);
        noerror (bar -> x);
    }
    if (blat == nullptr) {
        flagerror ();
    }
    noerror (blat -> z);
}

