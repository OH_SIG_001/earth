Prototype C++ Code Auditor
J.R. Cordy et al., Huawei Technologies

November 2023 (Revised Nov 2023)

This is a rapid prototype of a C++ code auditor tool to check
and advise on proper HOS C++ code usage.

To run the C++ code auditor on an input, use the command:
	txl app.cpp cppaudit.txl > app.cpp.audit.cpp
where "app.cpp" is the input C++ source file to be audited and "app.cpp.audit.cpp" 
is the audited output source file.

JRC 28.11.23
